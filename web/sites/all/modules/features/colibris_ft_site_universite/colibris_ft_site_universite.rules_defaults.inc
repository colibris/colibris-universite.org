<?php

/**
 * @file
 * colibris_ft_site_universite.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function colibris_ft_site_universite_default_rules_configuration() {
  $items = array();
  $items['rules_redirection_vers_la_page_utilisateur'] = entity_import('rules_config', '{ "rules_redirection_vers_la_page_utilisateur" : {
      "LABEL" : "Redirection vers la page utilisateur apres login",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_login" : [], "user_insert" : [] },
      "IF" : [
        { "NOT text_matches" : {
            "text" : [ "site:current-page:path" ],
            "match" : "^user\\/reset\\/",
            "operation" : "regex"
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "user", "force" : "0" } } ]
    }
  }');
  return $items;
}
