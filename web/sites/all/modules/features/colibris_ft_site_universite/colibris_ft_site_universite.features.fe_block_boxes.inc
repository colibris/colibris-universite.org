<?php

/**
 * @file
 * colibris_ft_site_universite.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function colibris_ft_site_universite_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Bandeau';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'bandeau';
  $fe_block_boxes->body = '<header class="heading" data-href="/" style="background-image: url(sites/all/themes/custom/colibrisuniversite/images/bandeau-default.jpg);"><div class="wrapper"><span class="name">Bienvenu-e-s sur le site de l&#39;université</span></div></header>';

  $export['bandeau'] = $fe_block_boxes;

  return $export;
}
