<?php

/**
 * @file
 * colibris_ft_site_universite.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function colibris_ft_site_universite_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view__panel_context_4783c4c9-ca36-42c1-b74a-eb5bbb3d9178';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Mon profil',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'mon-profil',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'did' => '1',
  );
  $export['user_view__panel_context_4783c4c9-ca36-42c1-b74a-eb5bbb3d9178'] = $handler;

  return $export;
}
