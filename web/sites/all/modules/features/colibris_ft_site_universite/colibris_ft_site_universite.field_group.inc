<?php

/**
 * @file
 * colibris_ft_site_universite.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function colibris_ft_site_universite_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_col_one_third|node|formation|default';
  $field_group->group_name = 'group_col_one_third';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '0',
    'children' => array(
      0 => 'field_type_de_formation',
      1 => 'field_date_d_inscription',
      2 => 'field_lien_vers_la_participation',
      3 => 'buttons',
      4 => 'date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-one-third field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_col_one_third|node|formation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_col_one_third|node|formation|form';
  $field_group->group_name = 'group_col_one_third';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '15',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-col-one-third field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_col_one_third|node|formation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image|node|formation|teaser';
  $field_group->group_name = 'group_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '0',
    'children' => array(
      0 => 'field_image',
      1 => 'date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image|node|formation|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info|node|formation|card';
  $field_group->group_name = 'group_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'card';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_image',
      2 => 'field_type_de_formation',
      3 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-info field-group-card',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_info|node|formation|card'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sticky|node|formation|card';
  $field_group->group_name = 'group_sticky';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'card';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '1',
    'children' => array(
      0 => 'buttons',
      1 => 'date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-sticky field-group-card',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_sticky|node|formation|card'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_360|node|formation|form';
  $field_group->group_name = 'group_training_360';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_training_info';
  $field_group->data = array(
    'label' => 'Formation 360 learning',
    'weight' => '18',
    'children' => array(
      0 => 'field_id_groupe_360learning',
      1 => 'learning360_id',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Formation 360 learning',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-training-360 field-group-htab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_training_360|node|formation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_info|node|formation|form';
  $field_group->group_name = 'group_training_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Informations de formation',
    'weight' => '8',
    'children' => array(
      0 => 'group_training_360',
      1 => 'group_training_third_party',
      2 => 'group_training_yeswiki',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-training-info field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_training_info|node|formation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_third_party|node|formation|form';
  $field_group->group_name = 'group_training_third_party';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_training_info';
  $field_group->data = array(
    'label' => 'Formation site tiers',
    'weight' => '21',
    'children' => array(
      0 => 'field_lien_vers_la_formation',
      1 => 'field_training_register_external',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-training-third-party field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_training_third_party|node|formation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_yeswiki|node|formation|form';
  $field_group->group_name = 'group_training_yeswiki';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_training_info';
  $field_group->data = array(
    'label' => 'Formation YesWiki',
    'weight' => '19',
    'children' => array(
      0 => 'field_lien_vers_le_wiki',
      1 => 'field_yeswiki_bearer',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-training-yeswiki field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_training_yeswiki|node|formation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_two_third|node|formation|default';
  $field_group->group_name = 'group_two_third';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'formation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_formateurs',
      2 => 'field_image',
      3 => 'field_th_matiques',
      4 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-two-third field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_two_third|node|formation|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Formation 360 learning');
  t('Formation YesWiki');
  t('Formation site tiers');
  t('Informations de formation');

  return $field_groups;
}
