<?php

/**
 * @file
 * colibris_ft_site_universite.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function colibris_ft_site_universite_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Economie',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '0048ed09-518f-477b-9ce5-8d4b596031da',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'Cursus professionnalisant',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '08d71119-5874-4de7-a892-c957bb765886',
    'vocabulary_machine_name' => 'type_de_formation',
    'field_training_pro' => array(),
  );
  $terms[] = array(
    'name' => 'Gouvernance',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '12be2191-bc1a-4cdb-9302-9b35a2d543f2',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'mooc',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1bf46ad5-1a68-4cbc-841a-40ec9d1c09c3',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'MOOC',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '238441f2-9073-44a1-ab1e-6a38ffc61845',
    'vocabulary_machine_name' => 'type_de_formation',
    'field_training_pro' => array(),
    'metatags' => array(
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
        'schema_organization.address' => array(
          'value' => 'a:5:{s:5:"@type";s:13:"PostalAddress";s:13:"streetAddress";s:26:"18-20 rue Euryale Dehaynin";s:15:"addressLocality";s:5:"Paris";s:10:"postalCode";s:5:"75019";s:14:"addressCountry";s:2:"FR";}',
        ),
        'schema_organization.geo' => array(
          'value' => 'a:3:{s:5:"@type";s:14:"GeoCoordinates";s:8:"latitude";s:10:"48.8862018";s:9:"longitude";s:9:"2.3755588";}',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Education',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '30f1c42c-39f4-4257-bf07-94a150954773',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'Utilisation de la plateforme d\'apprentissage en ligne',
    'description' => '',
    'format' => 'full_html',
    'weight' => 4,
    'uuid' => '3e7ec201-04b6-4ade-bec8-eaf8d868d35e',
    'vocabulary_machine_name' => 'categorie_faq',
  );
  $terms[] = array(
    'name' => 'Moocs',
    'description' => '<p>Massive Online Open Courses ou Formation en ligne ouvert à tous.</p>',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '3f5802c9-360b-4e60-9b75-6222c9b9dce5',
    'vocabulary_machine_name' => 'type_de_formation',
    'field_training_pro' => array(),
  );
  $terms[] = array(
    'name' => 'Utilisation du wiki et modification de vos informations',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '5f7ca1d6-8279-4033-9147-38b6b05e365c',
    'vocabulary_machine_name' => 'categorie_faq',
  );
  $terms[] = array(
    'name' => 'Agriculture & Alimentation',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '6bb2ca0e-45fa-438a-a543-dc6b982d2162',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'Habitat & Énergie',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7ccd27d7-da90-40aa-8125-df368d4c1089',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'Santé & Bien-être',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8351a7e4-63b0-4f27-bd55-9e4eadb5f241',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'transition',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a1b4bf20-0236-4e21-a955-c892ad9bd329',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'savoir-faire',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b054227d-e74e-42ca-9e7a-3509d8534266',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Arts & Culture',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'bdf1e5bc-eb39-4d81-9101-8c2438b162c1',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'Transformation intérieure',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'bfd67228-7819-474c-bbd3-1fae58c48a04',
    'vocabulary_machine_name' => 'thematiques_colibris',
  );
  $terms[] = array(
    'name' => 'formation en ligne',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c8e85f3e-e403-4118-8432-c290dfef5aaf',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'méthodes',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cb14e719-67b8-4bd4-a3eb-7750a321b8c3',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'université',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd31a3f2f-5305-4674-9d60-cc8f96f11c23',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Parcours citoyens',
    'description' => '<p>Parcours citoyens</p>
',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'ebd47a0b-1f69-4a86-a08c-14b6a61f3918',
    'vocabulary_machine_name' => 'type_de_formation',
    'field_training_pro' => array(),
    'metatags' => array(
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
        'schema_organization.address' => array(
          'value' => 'a:5:{s:5:"@type";s:13:"PostalAddress";s:13:"streetAddress";s:26:"18-20 rue Euryale Dehaynin";s:15:"addressLocality";s:5:"Paris";s:10:"postalCode";s:5:"75019";s:14:"addressCountry";s:2:"FR";}',
        ),
        'schema_organization.geo' => array(
          'value' => 'a:3:{s:5:"@type";s:14:"GeoCoordinates";s:8:"latitude";s:10:"48.8862018";s:9:"longitude";s:9:"2.3755588";}',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Utilisation du site de l\'Université',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'ebe71353-1d80-4bb4-a361-c4e9131324cd',
    'vocabulary_machine_name' => 'categorie_faq',
  );
  return $terms;
}
