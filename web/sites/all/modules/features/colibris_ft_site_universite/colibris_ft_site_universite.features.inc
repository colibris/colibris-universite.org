<?php

/**
 * @file
 * colibris_ft_site_universite.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function colibris_ft_site_universite_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function colibris_ft_site_universite_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function colibris_ft_site_universite_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['d.m.Y'] = 'd.m.Y';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function colibris_ft_site_universite_date_format_types() {
  $format_types = array();
  // Exported date format type: courte_sans_heure
  $format_types['courte_sans_heure'] = 'Courte sans heure';
  return $format_types;
}

/**
 * Implements hook_image_default_styles().
 */
function colibris_ft_site_universite_image_default_styles() {
  $styles = array();

  // Exported image style: card.
  $styles['card'] = array(
    'label' => 'Card',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 460,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: image_en_une.
  $styles['image_en_une'] = array(
    'label' => 'Image en une',
    'effects' => array(
      9 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1080,
          'height' => 620,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function colibris_ft_site_universite_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>articles</em> pour des contenus possédant une temporalité tels que des actualités, des communiqués de presse ou des billets de blog.'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'formation' => array(
      'name' => t('Formation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page de base'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>pages de base</em> pour votre contenu statique, tel que la page \'Qui sommes-nous\'.'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'zoom_sur_accueil' => array(
      'name' => t('Zoom sur accueil'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
