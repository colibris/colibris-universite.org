<?php

/**
 * @file
 * colibris_ft_site_universite.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function colibris_ft_site_universite_locale_default_languages() {
  $languages = array();

  // Exported language: fr.
  $languages['fr'] = array(
    'language' => 'fr',
    'name' => 'French',
    'native' => 'Français',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 2,
    'formula' => '($n>1)',
    'domain' => '',
    'prefix' => 'fr',
    'weight' => 0,
  );
  return $languages;
}
