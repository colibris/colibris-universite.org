<?php

/**
 * @file
 * colibris_ft_site_universite.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function colibris_ft_site_universite_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'Beta testeurs' => 'Beta testeurs',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access media browser'.
  $permissions['access media browser'] = array(
    'name' => 'access media browser',
    'roles' => array(
      'Beta testeurs' => 'Beta testeurs',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'media',
  );

  // Exported permission: 'add media from remote sources'.
  $permissions['add media from remote sources'] = array(
    'name' => 'add media from remote sources',
    'roles' => array(
      'Beta testeurs' => 'Beta testeurs',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'media_internet',
  );

  // Exported permission: 'administer media browser'.
  $permissions['administer media browser'] = array(
    'name' => 'administer media browser',
    'roles' => array(
      'Admin' => 'Admin',
    ),
    'module' => 'media',
  );

  // Exported permission: 'answer question'.
  $permissions['answer question'] = array(
    'name' => 'answer question',
    'roles' => array(
      'Admin' => 'Admin',
    ),
    'module' => 'faq_ask',
  );

  // Exported permission: 'ask question'.
  $permissions['ask question'] = array(
    'name' => 'ask question',
    'roles' => array(
      'Admin' => 'Admin',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'faq_ask',
  );

  // Exported permission: 'use media wysiwyg'.
  $permissions['use media wysiwyg'] = array(
    'name' => 'use media wysiwyg',
    'roles' => array(
      'Beta testeurs' => 'Beta testeurs',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'media_wysiwyg',
  );

  // Exported permission: 'view faq page'.
  $permissions['view faq page'] = array(
    'name' => 'view faq page',
    'roles' => array(
      'Admin' => 'Admin',
      'Beta testeurs' => 'Beta testeurs',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'faq',
  );

  // Exported permission: 'view overridden file entities in wysiwyg'.
  $permissions['view overridden file entities in wysiwyg'] = array(
    'name' => 'view overridden file entities in wysiwyg',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'media_ckeditor',
  );

  return $permissions;
}
