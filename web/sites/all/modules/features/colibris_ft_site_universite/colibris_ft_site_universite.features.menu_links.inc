<?php

/**
 * @file
 * colibris_ft_site_universite.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function colibris_ft_site_universite_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_actualits:actualités.
  $menu_links['main-menu_actualits:actualités'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'actualités',
    'router_path' => 'actualités',
    'link_title' => 'Actualités',
    'options' => array(
      'identifier' => 'main-menu_actualits:actualités',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_aide:faq-page.
  $menu_links['main-menu_aide:faq-page'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'faq-page',
    'router_path' => 'faq-page',
    'link_title' => 'Aide',
    'options' => array(
      'attributes' => array(
        'title' => 'Consultez la foire aux questions',
      ),
      'identifier' => 'main-menu_aide:faq-page',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_les-formations:formations.
  $menu_links['main-menu_les-formations:formations'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'formations',
    'router_path' => 'formations',
    'link_title' => 'Les formations',
    'options' => array(
      'attributes' => array(
        'title' => 'Toutes nos formations et celles des partenaires',
      ),
      'identifier' => 'main-menu_les-formations:formations',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_qui-sommes-nous:node/10.
  $menu_links['main-menu_qui-sommes-nous:node/10'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/10',
    'router_path' => 'node/%',
    'link_title' => 'Qui sommes nous',
    'options' => array(
      'attributes' => array(
        'title' => 'Qui sommes nous ?',
      ),
      'identifier' => 'main-menu_qui-sommes-nous:node/10',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-footer_contact:node/111.
  $menu_links['menu-menu-footer_contact:node/111'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'node/111',
    'router_path' => 'node/%',
    'link_title' => 'Contact',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-menu-footer_contact:node/111',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-footer_licence:node/117.
  $menu_links['menu-menu-footer_licence:node/117'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'node/117',
    'router_path' => 'node/%',
    'link_title' => 'Licence',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-menu-footer_licence:node/117',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-footer_mentions-lgales:node/127.
  $menu_links['menu-menu-footer_mentions-lgales:node/127'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'node/127',
    'router_path' => 'node/%',
    'link_title' => 'Mentions légales',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-menu-footer_mentions-lgales:node/127',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-footer_nous-soutenir:https://www.jedonneenligne.org/colibris/DONS/.
  $menu_links['menu-menu-footer_nous-soutenir:https://www.jedonneenligne.org/colibris/DONS/'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'https://www.jedonneenligne.org/colibris/DONS/',
    'router_path' => '',
    'link_title' => 'Nous soutenir',
    'options' => array(
      'attributes' => array(
        'title' => 'Nous soutenir',
      ),
      'identifier' => 'menu-menu-footer_nous-soutenir:https://www.jedonneenligne.org/colibris/DONS/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-footer_presse:node/104.
  $menu_links['menu-menu-footer_presse:node/104'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'node/104',
    'router_path' => 'node/%',
    'link_title' => 'Presse',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-menu-footer_presse:node/104',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-sites-colibris_colibris:https://www.colibris-lemouvement.org/.
  $menu_links['menu-sites-colibris_colibris:https://www.colibris-lemouvement.org/'] = array(
    'menu_name' => 'menu-sites-colibris',
    'link_path' => 'https://www.colibris-lemouvement.org/',
    'router_path' => '',
    'link_title' => 'Colibris',
    'options' => array(
      'attributes' => array(
        'title' => 'Le site du mouvement',
      ),
      'identifier' => 'menu-sites-colibris_colibris:https://www.colibris-lemouvement.org/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-sites-colibris_la-boutique:https://www.colibris-laboutique.org/.
  $menu_links['menu-sites-colibris_la-boutique:https://www.colibris-laboutique.org/'] = array(
    'menu_name' => 'menu-sites-colibris',
    'link_path' => 'https://www.colibris-laboutique.org/',
    'router_path' => '',
    'link_title' => 'La boutique',
    'options' => array(
      'attributes' => array(
        'title' => 'La boutique',
      ),
      'identifier' => 'menu-sites-colibris_la-boutique:https://www.colibris-laboutique.org/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-sites-colibris_la-fabrique:https://www.colibris-lafabrique.org/.
  $menu_links['menu-sites-colibris_la-fabrique:https://www.colibris-lafabrique.org/'] = array(
    'menu_name' => 'menu-sites-colibris',
    'link_path' => 'https://www.colibris-lafabrique.org/',
    'router_path' => '',
    'link_title' => 'La fabrique',
    'options' => array(
      'attributes' => array(
        'title' => 'La fabrique citoyenne des Colibris',
      ),
      'identifier' => 'menu-sites-colibris_la-fabrique:https://www.colibris-lafabrique.org/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-sites-colibris_luniversit:https://www.colibris-universite.org/.
  $menu_links['menu-sites-colibris_luniversit:https://www.colibris-universite.org/'] = array(
    'menu_name' => 'menu-sites-colibris',
    'link_path' => 'https://www.colibris-universite.org/',
    'router_path' => '',
    'link_title' => 'L\'université',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-sites-colibris_luniversit:https://www.colibris-universite.org/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-sites-colibris_outils-libres:https://www.colibris-outilslibres.org/.
  $menu_links['menu-sites-colibris_outils-libres:https://www.colibris-outilslibres.org/'] = array(
    'menu_name' => 'menu-sites-colibris',
    'link_path' => 'https://www.colibris-outilslibres.org/',
    'router_path' => '',
    'link_title' => 'Outils Libres',
    'options' => array(
      'attributes' => array(
        'title' => 'Colibris Outils Libres',
      ),
      'identifier' => 'menu-sites-colibris_outils-libres:https://www.colibris-outilslibres.org/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Actualités');
  t('Aide');
  t('Colibris');
  t('Contact');
  t('L\'université');
  t('La boutique');
  t('La fabrique');
  t('Les formations');
  t('Licence');
  t('Mentions légales');
  t('Nous soutenir');
  t('Outils Libres');
  t('Presse');
  t('Qui sommes nous');

  return $menu_links;
}
