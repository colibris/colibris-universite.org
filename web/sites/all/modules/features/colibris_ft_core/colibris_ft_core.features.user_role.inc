<?php

/**
 * @file
 * colibris_ft_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function colibris_ft_core_user_default_roles() {
  $roles = array();

  // Exported role: Admin.
  $roles['Admin'] = array(
    'name' => 'Admin',
    'weight' => 2,
  );

  return $roles;
}
