<?php

/**
 * @file
 * colibris_ft_360learning.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function colibris_ft_360learning_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
