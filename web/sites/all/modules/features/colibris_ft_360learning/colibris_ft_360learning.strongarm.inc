<?php

/**
 * @file
 * colibris_ft_360learning.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function colibris_ft_360learning_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'learning360_apikey';
  $strongarm->value = '0a7122af3bac419988767129e033f939';
  $export['learning360_apikey'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'learning360_company';
  $strongarm->value = '55f287847dacf77e47aab683';
  $export['learning360_company'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'learning360_host';
  $strongarm->value = 'https://colibris.360learning.com';
  $export['learning360_host'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'learning360_session_fields_mapping';
  $strongarm->value = array(
    'startDate' => '',
    'endDate' => '',
  );
  $export['learning360_session_fields_mapping'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'learning360_session_ntype';
  $strongarm->value = 'formation';
  $export['learning360_session_ntype'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'learning360_user_fields_mapping';
  $strongarm->value = array(
    'firstName' => 'field_first_name',
    'lastName' => 'field_last_name',
  );
  $export['learning360_user_fields_mapping'] = $strongarm;

  return $export;
}
