<?php

/**
 * @file
 * colibris_environments.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function colibris_environments_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "environment_indicator" && $api == "default_environment_indicator_environments") {
    return array("version" => "1");
  }
}
