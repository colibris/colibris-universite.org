<?php

/**
 * @file
 * colibris_ft_seo.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function colibris_ft_seo_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: global.
  $config['global'] = array(
    'instance' => 'global',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[current-page:title] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => 'Donner du pouvoir d\'agir à ceux qui veulent construire une société plus écologique et humaine.',
      ),
      'abstract' => array(
        'value' => '',
      ),
      'keywords' => array(
        'value' => '',
      ),
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'news_keywords' => array(
        'value' => '',
      ),
      'standout' => array(
        'value' => '',
      ),
      'rating' => array(
        'value' => '',
      ),
      'referrer' => array(
        'value' => '',
      ),
      'generator' => array(
        'value' => 'Drupal 7 (http://drupal.org)',
      ),
      'rights' => array(
        'value' => '',
      ),
      'image_src' => array(
        'value' => '',
      ),
      'canonical' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'shortlink' => array(
        'value' => '[current-page:url:unaliased]',
      ),
      'original-source' => array(
        'value' => '',
      ),
      'prev' => array(
        'value' => '',
      ),
      'next' => array(
        'value' => '',
      ),
      'content-language' => array(
        'value' => 'fr',
      ),
      'geo.position' => array(
        'value' => '',
      ),
      'geo.placename' => array(
        'value' => '',
      ),
      'geo.region' => array(
        'value' => '',
      ),
      'icbm' => array(
        'value' => '',
      ),
      'refresh' => array(
        'value' => '',
      ),
      'revisit-after' => array(
        'value' => '',
        'period' => '',
      ),
      'pragma' => array(
        'value' => '',
      ),
      'cache-control' => array(
        'value' => '',
      ),
      'expires' => array(
        'value' => '',
      ),
      'og:site_name' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'article',
      ),
      'og:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'og:title' => array(
        'value' => '[current-page:title]',
      ),
      'og:determiner' => array(
        'value' => '',
      ),
      'og:description' => array(
        'value' => 'Donner du pouvoir d\'agir à ceux qui veulent construire une société plus écologique et humaine.',
      ),
      'og:updated_time' => array(
        'value' => '',
      ),
      'og:see_also' => array(
        'value' => '',
      ),
      'og:image' => array(
        'value' => '',
      ),
      'og:image:url' => array(
        'value' => '',
      ),
      'og:image:secure_url' => array(
        'value' => '',
      ),
      'og:image:type' => array(
        'value' => '',
      ),
      'og:image:width' => array(
        'value' => '',
      ),
      'og:image:height' => array(
        'value' => '',
      ),
      'og:latitude' => array(
        'value' => '',
      ),
      'og:longitude' => array(
        'value' => '',
      ),
      'og:street_address' => array(
        'value' => '',
      ),
      'og:locality' => array(
        'value' => '',
      ),
      'og:region' => array(
        'value' => '',
      ),
      'og:postal_code' => array(
        'value' => '',
      ),
      'og:country_name' => array(
        'value' => '',
      ),
      'og:email' => array(
        'value' => '',
      ),
      'og:phone_number' => array(
        'value' => '',
      ),
      'og:fax_number' => array(
        'value' => '',
      ),
      'og:locale' => array(
        'value' => 'fr_FR',
      ),
      'og:locale:alternate' => array(
        'value' => '',
      ),
      'article:author' => array(
        'value' => '',
      ),
      'article:publisher' => array(
        'value' => 'https://fr-fr.facebook.com/MouvementColibris/',
      ),
      'article:section' => array(
        'value' => '',
      ),
      'article:tag' => array(
        'value' => '',
      ),
      'article:published_time' => array(
        'value' => '',
      ),
      'article:modified_time' => array(
        'value' => '',
      ),
      'article:expiration_time' => array(
        'value' => '',
      ),
      'profile:first_name' => array(
        'value' => '',
      ),
      'profile:last_name' => array(
        'value' => '',
      ),
      'profile:username' => array(
        'value' => '',
      ),
      'profile:gender' => array(
        'value' => '',
      ),
      'og:audio' => array(
        'value' => '',
      ),
      'og:audio:secure_url' => array(
        'value' => '',
      ),
      'og:audio:type' => array(
        'value' => '',
      ),
      'book:author' => array(
        'value' => '',
      ),
      'book:isbn' => array(
        'value' => '',
      ),
      'book:release_date' => array(
        'value' => '',
      ),
      'book:tag' => array(
        'value' => '',
      ),
      'og:video:url' => array(
        'value' => '',
      ),
      'og:video:secure_url' => array(
        'value' => '',
      ),
      'og:video:width' => array(
        'value' => '',
      ),
      'og:video:height' => array(
        'value' => '',
      ),
      'og:video:type' => array(
        'value' => '',
      ),
      'video:actor' => array(
        'value' => '',
      ),
      'video:actor:role' => array(
        'value' => '',
      ),
      'video:director' => array(
        'value' => '',
      ),
      'video:writer' => array(
        'value' => '',
      ),
      'video:duration' => array(
        'value' => '',
      ),
      'video:release_date' => array(
        'value' => '',
      ),
      'video:tag' => array(
        'value' => '',
      ),
      'video:series' => array(
        'value' => '',
      ),
      'schema_article.@type' => array(
        'value' => '',
      ),
      'schema_article.headline' => array(
        'value' => '',
      ),
      'schema_article.description' => array(
        'value' => '',
      ),
      'schema_article.author' => array(
        'value' => '',
      ),
      'schema_article.publisher' => array(
        'value' => '',
      ),
      'schema_article.datePublished' => array(
        'value' => '',
      ),
      'schema_article.dateModified' => array(
        'value' => '',
      ),
      'schema_article.about' => array(
        'value' => '',
      ),
      'schema_article.image' => array(
        'value' => '',
      ),
      'schema_article.mainEntityOfPage' => array(
        'value' => '',
      ),
      'schema_event.@type' => array(
        'value' => '',
      ),
      'schema_event.@id' => array(
        'value' => '',
      ),
      'schema_event.name' => array(
        'value' => '',
      ),
      'schema_event.image' => array(
        'value' => '',
      ),
      'schema_event.description' => array(
        'value' => '',
      ),
      'schema_event.url' => array(
        'value' => '',
      ),
      'schema_event.doorTime' => array(
        'value' => '',
      ),
      'schema_event.startDate' => array(
        'value' => '',
      ),
      'schema_event.endDate' => array(
        'value' => '',
      ),
      'schema_event.location' => array(
        'value' => '',
      ),
      'schema_event.offers' => array(
        'value' => '',
      ),
      'schema_event.performer' => array(
        'value' => '',
      ),
      'schema_item_list.@type' => array(
        'value' => '',
      ),
      'schema_item_list.@id' => array(
        'value' => '',
      ),
      'schema_item_list.itemListElement' => array(
        'value' => '',
      ),
      'schema_item_list.mainEntityOfPage' => array(
        'value' => '',
      ),
      'schema_organization.@type' => array(
        'value' => 'Organization',
      ),
      'schema_organization.@id' => array(
        'value' => '',
      ),
      'schema_organization.name' => array(
        'value' => 'Mouvement Colibris',
      ),
      'schema_organization.telephone' => array(
        'value' => '+33 1 42 15 50 17',
      ),
      'schema_organization.url' => array(
        'value' => 'https://www.colibris-lemouvement.org',
      ),
      'schema_organization.sameAs' => array(
        'value' => '',
      ),
      'schema_organization.address' => array(
        'value' => 'a:7:{s:5:"@type";s:13:"PostalAddress";s:13:"streetAddress";s:26:"18-20 rue Euryale Dehaynin";s:15:"addressLocality";s:5:"Paris";s:13:"addressRegion";s:0:"";s:10:"postalCode";s:5:"75019";s:14:"addressCountry";s:2:"FR";s:5:"pivot";s:0:"";}',
      ),
      'schema_organization.geo' => array(
        'value' => 'a:4:{s:5:"@type";s:14:"GeoCoordinates";s:8:"latitude";s:10:"48.8862018";s:9:"longitude";s:9:"2.3755588";s:5:"pivot";s:0:"";}',
      ),
      'schema_web_page.@type' => array(
        'value' => '',
      ),
      'schema_web_page.@id' => array(
        'value' => '',
      ),
      'schema_web_page.name' => array(
        'value' => '',
      ),
      'schema_web_page.url' => array(
        'value' => '',
      ),
    ),
  );

  // Exported Metatag config instance: global:frontpage.
  $config['global:frontpage'] = array(
    'instance' => 'global:frontpage',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[site:name] | [current-page:pager][site:slogan]',
      ),
      'description' => array(
        'value' => 'Donner du pouvoir d\'agir à ceux qui veulent construire une société plus écologique et humaine.',
      ),
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'image_src' => array(
        'value' => '[colibris:teaser_image]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:description' => array(
        'value' => 'Donner du pouvoir d\'agir à ceux qui veulent construire une société plus écologique et humaine.',
      ),
      'og:image' => array(
        'value' => '[colibris:teaser_image]',
      ),
      'og:image:width' => array(
        'value' => 1200,
      ),
      'og:image:height' => array(
        'value' => 1200,
      ),
    ),
  );

  // Exported Metatag config instance: node.
  $config['node'] = array(
    'instance' => 'node',
    'disabled' => FALSE,
    'config' => array(
      'description' => array(
        'value' => '[node:summary]',
      ),
      'og:description' => array(
        'value' => '[node:summary]',
      ),
      'og:updated_time' => array(
        'value' => '[node:changed:custom:c]',
      ),
      'article:published_time' => array(
        'value' => '[node:created:custom:c]',
      ),
      'article:modified_time' => array(
        'value' => '[node:changed:custom:c]',
      ),
      'schema_web_page.@type' => array(
        'value' => 'WebPage',
      ),
      'schema_web_page.name' => array(
        'value' => '[node:title]',
      ),
      'schema_web_page.url' => array(
        'value' => '[node:url]',
      ),
    ),
  );

  // Exported Metatag config instance: node:article.
  $config['node:article'] = array(
    'instance' => 'node:article',
    'disabled' => FALSE,
    'config' => array(
      'keywords' => array(
        'value' => '[node:field_tags]',
      ),
      'robots' => array(
        'value' => array(
          'index' => 'index',
          'follow' => 'follow',
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'news_keywords' => array(
        'value' => '[node:field_tags]',
      ),
      'image_src' => array(
        'value' => '[node:field_image:social_network]',
      ),
      'og:image' => array(
        'value' => '[node:field_image:social_network]',
      ),
      'og:image:width' => array(
        'value' => '[node:field_image:social_network:width]',
      ),
      'og:image:height' => array(
        'value' => '[node:field_image:social_network:height]',
      ),
      'article:tag' => array(
        'value' => '[node:field_tags]',
      ),
      'schema_article.@type' => array(
        'value' => 'NewsArticle',
      ),
      'schema_article.headline' => array(
        'value' => '[node:title]',
      ),
      'schema_article.description' => array(
        'value' => '[node:summary]',
      ),
      'schema_article.datePublished' => array(
        'value' => '[node:created:custom:Y-m-d\\TH:i:sO]',
      ),
      'schema_article.dateModified' => array(
        'value' => '[node:changed:custom:Y-m-d\\TH:i:sO]',
      ),
      'schema_article.about' => array(
        'value' => '[node:field_tags]',
      ),
      'schema_article.image' => array(
        'value' => 'a:4:{s:5:"@type";s:11:"ImageObject";s:3:"url";s:18:"[node:field_image]";s:5:"width";s:0:"";s:6:"height";s:0:"";}',
      ),
      'schema_article.mainEntityOfPage' => array(
        'value' => '[node:url]',
      ),
      'schema_web_page.@type' => array(
        'value' => '',
      ),
    ),
  );

  // Exported Metatag config instance: node:faq.
  $config['node:faq'] = array(
    'instance' => 'node:faq',
    'disabled' => FALSE,
    'config' => array(
      'keywords' => array(
        'value' => '[node:field_categorie]',
      ),
      'robots' => array(
        'value' => array(
          'index' => 'index',
          'follow' => 'follow',
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'article:tag' => array(
        'value' => '[node:field_categorie]',
      ),
    ),
  );

  // Exported Metatag config instance: node:formation.
  $config['node:formation'] = array(
    'instance' => 'node:formation',
    'disabled' => FALSE,
    'config' => array(
      'keywords' => array(
        'value' => '[node:field_th_matiques]',
      ),
      'robots' => array(
        'value' => array(
          'index' => 'index',
          'follow' => 'follow',
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'news_keywords' => array(
        'value' => '[node:field_th_matiques]',
      ),
      'image_src' => array(
        'value' => '[node:field_image:social_network]',
      ),
      'og:image' => array(
        'value' => '[node:field_image:social_network]',
      ),
      'og:image:width' => array(
        'value' => '[node:field_image:social_network:width]',
      ),
      'og:image:height' => array(
        'value' => '[node:field_image:social_network:height]',
      ),
      'article:tag' => array(
        'value' => '[node:field_th_matiques]',
      ),
      'schema_event.@type' => array(
        'value' => 'EducationEvent',
      ),
      'schema_event.name' => array(
        'value' => '[node:title]',
      ),
      'schema_event.image' => array(
        'value' => 'a:4:{s:5:"@type";s:11:"ImageObject";s:3:"url";s:18:"[node:field_image]";s:5:"width";s:0:"";s:6:"height";s:0:"";}',
      ),
      'schema_event.description' => array(
        'value' => '[node:summary]',
      ),
      'schema_event.url' => array(
        'value' => '[node:url]',
      ),
      'schema_event.doorTime' => array(
        'value' => '[node:field_date_d_inscription]',
      ),
      'schema_web_page.@type' => array(
        'value' => '',
      ),
    ),
  );

  // Exported Metatag config instance: node:zoom_sur_accueil.
  $config['node:zoom_sur_accueil'] = array(
    'instance' => 'node:zoom_sur_accueil',
    'disabled' => FALSE,
    'config' => array(
      'robots' => array(
        'value' => array(
          'noindex' => 'noindex',
          'nofollow' => 'nofollow',
          'noarchive' => 'noarchive',
          'nosnippet' => 'nosnippet',
          'noodp' => 'noodp',
          'noydir' => 'noydir',
          'noimageindex' => 'noimageindex',
          'notranslate' => 'notranslate',
          'index' => 0,
          'follow' => 0,
        ),
      ),
    ),
  );

  // Exported Metatag config instance: taxonomy_term.
  $config['taxonomy_term'] = array(
    'instance' => 'taxonomy_term',
    'disabled' => FALSE,
    'config' => array(
      'description' => array(
        'value' => '[term:description]',
      ),
      'og:description' => array(
        'value' => '[term:description]',
      ),
    ),
  );

  // Exported Metatag config instance: view.
  $config['view'] = array(
    'instance' => 'view',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[view:title] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => '[view:description]',
      ),
      'canonical' => array(
        'value' => '[view:url]',
      ),
    ),
  );

  return $config;
}
