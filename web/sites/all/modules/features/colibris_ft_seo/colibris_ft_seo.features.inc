<?php

/**
 * @file
 * colibris_ft_seo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function colibris_ft_seo_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function colibris_ft_seo_image_default_styles() {
  $styles = array();

  // Exported image style: social_network.
  $styles['social_network'] = array(
    'label' => 'Social network',
    'effects' => array(
      7 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 1200,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
