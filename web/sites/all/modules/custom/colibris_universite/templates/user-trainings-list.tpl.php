<div id="user-trainings-list-tabs">
  <ul>
    <li><a href="#formations-encours">Mes formations en cours</a></li>
    <li><a href="#formations-complet">Toutes mes formations</a></li>
  </ul>
  <div id="formations-encours">
    <?php print drupal_render($ongoing); ?>
  </div>
  <div id="formations-complet"
    <?php print drupal_render($subscribed); ?>
  </div>
</div>
