<?php
/**
 * Implements hook_token_info().
 */
function colibris_universite_token_info() {
  return array(
    'types' => array(
      'colibris' => array(
        'name' => t('Colibris'),
        'description' => t('Tokens related to Colibris content.'),
        'needs-data' => 'node',
      ),
    ),
    'tokens' => array(
      'colibris' => array(
        'teaser_image' => array(
          'name' => t('Teaser image'),
          'description' => t('Teaser content image.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function colibris_universite_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ('colibris' == $type) {
    foreach ($tokens as $name => $original) {
      switch($name) {
        case 'teaser_image':
          $teaser_media = _colibris_universite_get_teaser_media(TRUE);
          if (empty($teaser_media)) {
            break;
          }

          $teaser_uri = NULL;
          switch($teaser_media->type) {
            case 'image':
              $teaser_uri = $teaser_media->uri;
              break;

            case 'video':
              $wrapper = file_stream_wrapper_get_instance_by_uri($teaser_media->uri);
              $thumbnail = !empty($wrapper) ? $wrapper->getLocalThumbnailPath() : NULL;
              if (!empty($thumbnail)) {
                $teaser_uri = $thumbnail;
              }
              break;
          }

          if (!empty($teaser_uri)) {
            $replacements[$original] = image_style_url(COLIBRIS_SOCIAL_STYLE, $teaser_uri);
          }
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Get teaser content media.
 *
 * @param bool $load Load file entity.
 *
 * @return object|int Teaser media.
 */
function _colibris_universite_get_teaser_media($load = FALSE) {
  $query = db_select('node', 'n')
    ->addTag('node_access')
    ->condition('n.type', COLIBRIS_TEASER_NTYPE)
    ->condition('n.status', NODE_PUBLISHED)
    ->orderBy('n.promote', 'DESC')
    ->orderBy('n.created', 'DESC');

  $query->join('field_data_' . COLIBRIS_TEASER_FIELD, 'f', 'f.entity_id = n.nid');
  $query->fields('f', array(COLIBRIS_TEASER_FIELD . '_fid'));

  $teaser_fid = $query
    ->range(0, 1)
    ->execute()
    ->fetchField();

  return $load && !empty($teaser_fid) ? file_load($teaser_fid) : $teaser_fid;
}