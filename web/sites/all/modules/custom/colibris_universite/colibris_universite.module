<?php
define('COLIBRIS_TEASER_NTYPE', 'formation');
define('COLIBRIS_TEASER_FIELD', 'field_image');
define('COLIBRIS_SOCIAL_STYLE', 'social_network');
define('TRAINING_INCOMING', 1);
define('TRAINING_OVER', 2);
define('TRAINING_REGISTRABLE', 3);
define('TRAINING_ALWAYS_ACCESSIBLE', 4);

/* ajout de la vue card */
function colibris_universite_entity_info_alter(&$info)
{
    // Add the 'Card' view mode for nodes.
    $info['node']['view modes']['card'] = array(
        'label' => t('Card'),
        'custom settings' => true,
    );
}

/* possibilite bouger le titre, les boutons de reservation, et la date pour la vue card */
function colibris_universite_field_extra_fields()
{
    // formations
    $extra['node']['formation'] = array(
        'display' => array(
            'title' => array(
                'label' => t('Titre'),
                'description' => t('Affiche le titre'),
                'weight' => 0,
            ),
            'buttons' => array(
                'label' => t('Boutons'),
                'description' => t('Boutons de réservation'),
                'weight' => 100,
            ),
            'date' => array(
                'label' => t('Date'),
                'description' => t('Date des formations'),
                'weight' => 101,
            ),
        )
    );

    // actualites
    $extra['node']['article'] = array(
        'display' => array(
            'title' => array(
                'label' => t('Titre'),
                'description' => t('Affiche le titre'),
                'weight' => 0,
            ),
            'more-link' => array(
                'label' => t('Lien "En savoir plus"'),
                'description' => t('Lien vers l\'article intégral'),
                'weight' => 101,
            ),
            'publication-date' => array(
                'label' => t('Date de publication'),
                'description' => t('Date de publication de l\'article'),
                'weight' => 10,
            ),
        )
    );
    return $extra;
}

/**
 * Implements hook_entity_load().
 */
function colibris_universite_entity_load($entities, $type) {
  if ($type == 'node') {
    foreach ($entities as $nid => $node) {
      if ($node->type == 'formation') {
        $content_link = FALSE;
        $external_registration_link = FALSE;

        // Build training registration links.
        // @TODO verify if we should use group ID or ID to determine if we
        // are accessing to a 360 learning training.
        if (_field_has_content($node->{LEARNING360_ID_FIELD})) {
          $training_type = '360learning';
          $registration_link = 'formation/inscription/360learning/' . $nid;
          $unregistration_link = 'formation/desinscription/360learning/' . $nid;
          $content_link = 'https://colibris.360learning.com/program/' . $node->{LEARNING360_ID_FIELD}[LANGUAGE_NONE][0]['value'];
        }
        // Yes wiki sessions.
        elseif (_field_has_content($node->field_lien_vers_le_wiki)) {
          $training_type = 'yes_wiki';
          $registration_link = 'formation/inscription/yes_wiki/' . $nid;
          $unregistration_link = 'formation/desinscription/yes_wiki/' . $nid;
          $content_link = $node->field_lien_vers_le_wiki[LANGUAGE_NONE][0]['url'];
        }
        // External trainings.
        else {
          $training_type = 'external';
          $registration_link = 'formation/inscription/external/' . $nid;
          $unregistration_link = 'formation/desinscription/external/' . $nid;

          if (_field_has_content($node->field_lien_vers_la_formation) > 0) {
            $content_link = $node->field_lien_vers_la_formation[LANGUAGE_NONE][0]['value'];
          }

          if (_field_has_content($node->field_training_register_external)) {
            $external_registration_link = $node->field_training_register_external[LANGUAGE_NONE][0]['url'];
          }
        }
        $node->training_type = $training_type;
        $node->training_external_registration_link = $external_registration_link;
        $node->training_registration_link = $registration_link;
        $node->training_unregistration_link = $unregistration_link;
        $node->training_content_link = $content_link;

        // Check which training category is the content labelled as since
        // "Parcours" are always accessible.
        $training_category = NULL;
        if (_field_has_content($node->field_type_de_formation)) {
          $category = taxonomy_term_load($node->field_type_de_formation[LANGUAGE_NONE][0]["tid"]);
          $training_category = strtolower($category->name);
        }
        if (!empty($training_category) && in_array($training_category, ['parcours citoyens'])) {
          $training_access_status = TRAINING_ALWAYS_ACCESSIBLE;
        }
        else {
          // Determine the training access status.
          $today = date('Y-m-d');
          $training_access_status = TRAINING_REGISTRABLE;

          $date = field_get_items('node', $node, 'field_date_d_inscription');
          if ($date) {
            $firstday = date('Y-m-d', strtotime($date[0]['value']));
            $lastday = date('Y-m-d', strtotime($date[0]['value2']));
            // Firstday and lastday are equals if you don't select an end date.
            if ($firstday != $lastday && $today > $lastday) {
              $training_access_status = TRAINING_OVER;
            }
            elseif ($today < $firstday) { // la date est future
              $training_access_status = TRAINING_INCOMING;
            }
          }
        }
        $node->training_access_status = $training_access_status;

        // Add an attribute to identify which target is the training for.
        $training_target = 'public';
        if (!empty($training_category) && in_array($training_category, ['parcours organisations', 'formation présentiel'])) {
          $training_target = 'company';
        }
        $node->training_target = $training_target;
      }
    }
  }
}

function colibris_universite_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'formation') {
    if ($view_mode == 'full') {
      $markup = '<h2 class="title">' . $node->title . '</h2>';

      // Warn the admins if the wiki has content but no bearer defined.
      // We need it to acces to YesWiki API.
      if (_field_has_content($node->field_lien_vers_le_wiki) && !_field_has_content($node->field_yeswiki_bearer)) {
        drupal_set_message("L'inscription au wiki n'est pas possible car la clé d'accès n'a pas été renseignée. Merci de compléter le champ de cette formation.", "error");
        return;
      }
    }
    else {
      $markup = '<h2 class="title"><a href="/' . drupal_get_path_alias('node/' . $node->nid) . '">' . $node->title . '</a></h2>';
    }

    // mise en forme du titre des formations
    $node->content['title'] = [
      "#markup" => $markup,
      "#weight" => 0,
    ];

    // ajout d'une classe pour les formations
    $formation = '';
    if ($items = field_get_items('node', $node, 'field_type_de_formation')) {
      $formation = strtolower($items[0]['taxonomy_term']->name);
    }

    if ($view_mode == 'full' or $view_mode == 'teaser') {
      $node->content['field_type_de_formation'][0]['#markup'] = '<span class="' . $formation . '">' . $node->content['field_type_de_formation'][0]['#markup'] . '</span>';
    }
    $node->content['field_type_de_formation']['#attributes']['class'][] = $formation;

    $node->content['date'] = [
      "#markup" => _training_date_markup($node, $formation),
      "#weight" => 100,
    ];
    // @TODO: Refactor this to be easier to decipher.
    // In the end we have two links on the card view mode, N links on the teaser
    // view mode and all links in the full view mode.
    if (in_array($view_mode, ['teaser', 'card']) && in_array($node->training_access_status, [TRAINING_INCOMING, TRAINING_OVER])) {
      $node->content['buttons'] = _training_more_information_markup($node->nid, $node->training_access_status);
    }
    else {
      if ($node->training_target == 'company') {
        $node->content['buttons'] = _training_build_actions_buttons($node, ['contact_us']);
      }
      else {
        if ($view_mode == 'card') {
          $node->content['buttons'] = _training_build_actions_buttons($node, ['external_subscription', 'subscription']);
        }
        else {
          $node->content['buttons'] = _training_build_actions_buttons($node, ['external_subscription', 'training_content', 'subscription']);
        }
      }
    }
    return $node;
  }

  if ($node->type == 'article') {
    if ($view_mode == 'card') {
      // mise en forme du titre des actus
      $node->content['title'] = [
        "#markup" => '<h2><a href="/' . drupal_get_path_alias('node/' . $node->nid) . '">' . $node->title . '</a></h2>',
        "#weight" => 0,
      ];

      // couper le body
      $shortbody = tokenTruncate(strip_tags($node->body['und'][0]['value']), 360);
      $node->content['body'] = [
        "#markup" => '<p class="short-content">' . $shortbody . '</p>',
        "#weight" => 1,
      ];

      // lien en savoir +
      $node->content['more-link'] = [
        "#markup" => '<a class="link-full-article" href="/' . drupal_get_path_alias('node/' . $node->nid) . '">En savoir +</a>',
        "#weight" => 1,
      ];
    }
    // publication-date
    $publication_date_render_array = ['#markup' => 'Publié le ' . format_date($node->changed, 'courte_sans_heure')];
    $node->content['publication-date'] = [
      '#theme' => 'field',
      '#label_display' => 'hidden',
      '#object' => $node,
      '#entity_type' => $node->type,
      '#language' => $langcode,
      '#view_mode' => $view_mode,
      '#field_name' => 'extra-publication-date',
      '#field_type' => 'extra-field',
      '#items' => [$publication_date_render_array],
    ];
    $node->content['publication-date'][] = $publication_date_render_array;
    return $node;
  }
}

/**
 * Helper to generate the More information link of a training.
 *
 * @param $training_nid
 *   Training Node ID.
 * @param $training_access_status
 *   Training access status const.
 *
 * @return array
 *   Return link render array.
 */
function _training_more_information_markup($training_nid, $training_access_status) {
  $link_classes = ['reservation'];
  switch ($training_access_status) {
    case TRAINING_INCOMING:
      $link_classes[] = 'a_venir';
      break;
    case TRAINING_OVER:
      $link_classes[] = 'termine';
      break;
  }
  $button = [[
    '#theme' => 'link',
    '#text' => 'En savoir +',
    '#path' => drupal_get_path_alias('node/' . $training_nid),
    '#options' => [
      'html' => TRUE,
      'attributes' => ['class' => $link_classes],
    ],
    '#weight' => 101,
  ]];
  return $button;
}

/**
 * Helper to build the training status output.
 *
 * @param $training
 *   Node object of the training.
 * @param $training_type
 *   Taxonomy term name of the given training type.
 *
 * @return mixed|string
 */
function _training_date_markup($training, $training_type) {
  $markup = '';
  switch($training->training_access_status) {
    case TRAINING_INCOMING:
      if (_field_has_content($training->field_date_d_inscription)) {
        $firstday = date('Y-m-d', strtotime($training->field_date_d_inscription[LANGUAGE_NONE][0]['value']));
        $lastday = date('Y-m-d', strtotime($training->field_date_d_inscription[LANGUAGE_NONE][0]['value2']));
        if ($lastday != $firstday) {
          $markup = '<div class="date a_venir"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Du ' . date('d.m.Y', strtotime($firstday)) . ' au ' . date('d.m.Y', strtotime($lastday)) . '</div>';
        }
        else {
          $markup = '<div class="date a_venir"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> À partir du ' . date('d.m.Y', strtotime($firstday)) . '</div>';
        }
      }
      else {
        $markup = '<div class="date a_venir"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> A venir</div>';
      }
      break;
    case TRAINING_OVER:
      $markup = '<div class="date termine"><i class="fa fa-history" aria-hidden="true"></i> Terminé</div>';
      break;
    default:
      $markup = '<div class="date en_cours"><i class="fa fa-hourglass-half" aria-hidden="true"></i> En cours</div>';
      break;
  }

  return $markup;
}

/**
 * Helper to build training actions buttons.
 *
 * @param $training
 *   Node to extract data from.
 * @param $link_types
 *   Array of buttons to generate. Allow values are: subscription,
 *   training_content, external_subscription, contact_us.
 *
 * @return array
 *   Array of $text, $path and $query arguments to passe to a #type link
 *   renderable element.
 */
function _training_build_actions_buttons($training, $link_types) {
  $weight = 101;
  $buttons = [];
  foreach ($link_types as $link_type) {
    switch ($link_type) {
      case 'subscription':
        list($text, $path, $query) = _training_subscription_button($training);
        break;
      case 'training_content':
        list($text, $path, $query) = _training_content_button($training);
        break;
      case 'external_subscription':
        list($text, $path, $query) = _training_external_subscription_button($training);
        break;
      case 'contact_us':
        list($text, $path, $query) = _training_contact_us_button($training);
        break;
    }
    if (!empty($path)) {
      $button = [
        '#theme' => 'link',
        '#text' => $text,
        '#path' => $path,
        '#options' => [
          'html' => TRUE,
          'query' => $query,
          'attributes' => ['class' => ['reservation', 'en_cours']],
        ],
        '#weight' => $weight,
      ];
      $buttons[] = $button;
      $weight++;
    }
  }
  return $buttons;
}

/**
 * Helper to generate the training subscription/unsubscription link.
 *
 * @param $training
 *   Node object.
 *
 * @return array
 * @throws \Exception
 */
function _training_subscription_button($training) {
  $query = [];
  $path = NULL;
  $text = NULL;
  if (in_array($training->training_access_status, [TRAINING_REGISTRABLE, TRAINING_ALWAYS_ACCESSIBLE])) {
    if (empty($GLOBALS['user']->uid)) {
      // Anonymous user
      $path = 'cas';
      $text = '<i class="fa fa-pencil" aria-hidden="true"></i> ' . "S'inscrire";
      $query = [
        'destination' => $training->training_registration_link,
      ];
    }
    elseif ($subscribed = _training_is_user_subscribed($training)) {
      // Unsubscription link.
      $query = [];
      $path = $training->training_unregistration_link;

      if ($training->training_type == 'external') {
        $text = '<i class="fa fa-minus" aria-hidden="true"></i> ' . "Retirer de mon tableau de bord";
      }
      else {
        $text = '<i class="fa fa-minus" aria-hidden="true"></i> Me désinscrire';
      }
    }
    else {
      // Subscription link.
      if ($training->training_type == 'external') {
        $text = '<i class="fa fa-plus" aria-hidden="true"></i> ' . "&Eacute;pingler";
      }
      else {
        $text = '<i class="fa fa-pencil" aria-hidden="true"></i> ' . "S'inscrire";
      }
      $path = $training->training_registration_link;
    }
  }
  return [$text, $path, $query];
}

/**
 * Helper to generate the training content link.
 *
 * @param $training
 *   Node object.
 *
 * @return array
 * @throws \Exception
 */
function _training_content_button($training) {
  $query = [];
  $path = NULL;
  $text = NULL;
  if ($subscribed = _training_is_user_subscribed($training)) {
    $view_content = TRUE;
    $today = date('Y-m-d');
    $datevisibilite = field_get_items('node', $training, 'field_date_limite_de_visibilite');

    // on affiche le bouton pour voir le cours si la date limite de visibilité n'est pas atteinte
    if ($datevisibilite && ($today >= date('Y-m-d', strtotime($datevisibilite[0]['value'])))) {
      $view_content = FALSE;
    }
    if ($view_content) {
      $path = $training->training_content_link;
      $text = '<i class="fa fa-book" aria-hidden="true"></i> Voir le cours';
    }
  }
  return [$text, $path, $query];
}

/**
 * Helper to generate the training external subscription/unsubscription link.
 *
 * @param $training
 *   Node object.
 *
 * @return array
 * @throws \Exception
 */
function _training_external_subscription_button($training) {
  $query = [];
  $path = NULL;
  $text = NULL;
  if (!$subscribed = _training_is_user_subscribed($training)) {
    $path = $training->training_external_registration_link;
    $text = '<i class="fa fa-pencil" aria-hidden="true"></i> ' . "S'inscrire";
  }
  return [$text, $path, $query];
}

/**
 * Helper to generate the training contact page link.
 *
 * @param $training
 *   Node object.
 *
 * @return array
 * @throws \Exception
 */
function _training_contact_us_button($training) {
  $query = [];
  $path = 'demander-un-devis';
  if (!empty($training->content['field_lien_vers_le_wiki']) && !empty($training->content['field_lien_vers_le_wiki']['#items'][0]['url'])) {
    $path = $training->content['field_lien_vers_le_wiki']['#items'][0]['url'];
  }
  $text = '<i class="fa fa-envelope" aria-hidden="true"></i> Nous contacter';

  return [$text, $path, $query];
}

function colibris_universite_block_view($delta = '') {
  if ($delta == 'newsletter') {
        $block = array(
            'subject' => '',
            'content' => array(
                '#markup' => '<p>' . t('This is some text that will go in the block.') . '</p>',
                // '#attached' => array(
                //     'js' => array('path/to/file.js'),
                //     'css' => array('path/to/file.css'),
                // ),
            ),
        );
        return $block;
    }
}

/**
 * Implements hook_menu().
 */
function colibris_universite_menu() {
  $items['formation/%/%/%node'] = [
    'page callback' => 'colibris_universite_subscription_manage',
    'page arguments' => [1, 2, 3],
    'access arguments' => ['update own training subscription status'],
  ];
  return $items;
}

/**
 * Menu callback to handle training subscriptions.
 *
 * @param string $op
 *   Action to perform: "subscribe" or "unsubscribe".
 * @param string $training_type
 *   Supported values: "yes_wiki", "external".
 * @param $training
 *   Node of the training to subscribe/unsubscribe to.
 *
 * @return string
 */
function colibris_universite_subscription_manage($op, $training_type, $training) {
  if (!in_array($op, ['inscription', 'desinscription'])) {
    $args = [
      '!op' => $op,
      '!nid' => $training->nid,
      '!uid' => $GLOBALS['user']->uid
    ];
    watchdog('colibris universite', 'Unsupported operation "!op" on training !nid for user !uid', $args, WATCHDOG_ERROR);
    throw new Exception('Unsupported operation to manage training subscription.');
  }
  $message = _update_user_training_subscriptions($op, $training);
  if (!empty($message)) {
    drupal_set_message(t($message, ['!title' => $training->title]));
  }
  switch ($training_type) {
    case '360learning':
      module_load_include('inc', 'learning360', 'includes/learning360.node');
      if ($op == 'inscription') {
        if (_field_has_content($training->{LEARNING360_GROUP_FIELD})) {
          learning360_node_subscribe_to_group($training);
        }
        else {
          learning360_node_subscribe($training);
        }
      }
      else {
        if (_field_has_content($training->{LEARNING360_GROUP_FIELD})) {
          learning360_node_unsubscribe_to_group($training);
        }
        else {
          learning360_node_unsubscribe($training);
        }
      }
      break;
    case 'yes_wiki':
      if (_field_has_content($training->field_yeswiki_bearer)) {
        // Here will come the code calling YesWiki API to notify about sub/unsub.
        $request_options = [
          'method' => 'POST',
          'headers' => [
            'Authorization' => 'Bearer ' . $training->field_yeswiki_bearer[LANGUAGE_NONE][0]['value'],
            'Accept' => 'application/json',
          ],
        ];
        $user_raw_data = user_load($GLOBALS['user']->uid);
        unset($user_raw_data->pass);
        unset($user_raw_data->data);

        // Extract URL path 1st fragment from something that looks like that:
        // /mooc-gouvernance/wakka.php.
        $training_url = parse_url($training->training_content_link);
        $url_path = substr($training_url['path'], 0, strpos($training_url['path'], '/', 1));
        $api_root_url = $training_url['scheme'] . '://' . $training_url['host'] . $url_path;
        if ($op == 'inscription') {
          $request_url = $api_root_url . '/?api/participant/subscribe';
          $request_options += [
            'data' => json_encode([
              'email' => $user_raw_data->mail,
              'name' => $user_raw_data->name,
              'data' => $user_raw_data,
            ]),
          ];
        }
        else {
          $request_url = $api_root_url . '/?api/participant/unsubscribe';
          $request_options += [
            'data' => json_encode([
              'email' => $user_raw_data->mail,
              'name' => $user_raw_data->name,
            ]),
          ];
        }
        $response = drupal_http_request($request_url , $request_options);
        if ($response->code != 200) {
          $args = [
            '!op' => $op,
            '!url' => $training_url,
            '!name' => $user_raw_data->name,
            '!uid' => $user_raw_data->uid,
          ];
          watchdog('colibris universite', 'Action "!op" failed for Yes Wiki (!url) for user !name #!uid', $args, WATCHDOG_ERROR);
          drupal_set_message("Votre " . $op . " n'a pas pu être mise à jour sur la plateforme, merci de le signaler à un administrateur.", 'error');
        }
      }
      break;
    case 'external':
      break;
    default:
      $args = [
        '!type' => $training_type,
        '!nid' => $training->nid,
        '!uid' => $GLOBALS['user']->uid
      ];
      watchdog('colibris universite', 'Unsupported training type "!type" on training !nid for user !uid', $args, WATCHDOG_ERROR);
      throw new Exception('Unsupported operation to manage training subscription.');
      break;
  }
  return drupal_goto('node/' . $training->nid);
}

/**
 * Helper to update user training subscriptions.
 *
 * @param string $op
 *   Action to perform: "subscribe" or "unsubscribe".
 * @param $training
 *   Node of the training to subscribe/unsubscribe to.
 *
 * @return string
 * @throws \Exception
 */
function _update_user_training_subscriptions($op, $training) {
  $save = FALSE;
  $message = NULL;
  $user = user_load($GLOBALS['user']->uid);
  if ($op == 'inscription') {
    $save = _training_build_subscribed_user_trainings($user, $training->nid);
  }
  else {
    // Unsubscribe user from training.
    if (!empty($user->field_training_sessions) && count($user->field_training_sessions[LANGUAGE_NONE]) > 0) {
      // Find which training to remove for the user.
      foreach ($user->field_training_sessions[LANGUAGE_NONE] as $delta => $subscribed_training) {
        if ($subscribed_training['target_id'] == $training->nid) {
          unset($user->field_training_sessions[LANGUAGE_NONE][$delta]);
          $save = TRUE;
          continue;
        }
      }
    }
  }
  if ($save) {
    user_save($user);
    if ($op == 'inscription') {
      $message = 'Votre inscription est maintenant effective pour la formation "!title"';
    }
    else {
      $message = 'Votre désinscription pour la formation "!title" a bien été prise en compte.';
    }
  }
  return $message;
}

/**
 * @param $user
 * @param $training_nid
 *
 * @return bool
 */
function _training_build_subscribed_user_trainings(&$user, $training_nid) {
  $save = FALSE;
  if (!empty($training_nid)) {
    if (!empty($user->field_training_sessions) && count($user->field_training_sessions[LANGUAGE_NONE]) > 0) {
      $training_already_subscribed = FALSE;
      // Verify that we do not duplicate the training subscription.
      foreach ($user->field_training_sessions[LANGUAGE_NONE] as $delta => $subscribed_training) {
        if ($subscribed_training['target_id'] == $training_nid) {
          $training_already_subscribed = TRUE;
          continue;
        }
      }
      if (!$training_already_subscribed) {
        $user->field_training_sessions[LANGUAGE_NONE][] = ['target_id' => $training_nid];
        $save = TRUE;
      }
    }
    else {
      $user->field_training_sessions = [LANGUAGE_NONE => [['target_id' => $training_nid]]];
      $save = TRUE;
    }
  }
  return $save;
}

// Enlever le contenu par defaut de la page d'accueil
function colibris_universite_menu_alter(&$items) {
  $items['node']['page callback'] = 'empty_front_page_callback';
}

/**
 * Implements hook_permission().
 */
function colibris_universite_permission() {
  return [
    'update own training subscription status' => [
      'title' => 'Update own training subscription status',
      'description' => 'Let users change their subscribed/unsubscribed status to a training',
    ]
  ];
}

function empty_front_page_callback()
{
    drupal_set_title('');
    return array();
}


// Tronquer au mot pres
function tokenTruncate($string, $your_desired_width)
{
  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
  $parts_count = count($parts);

  $length = 0;
  $last_part = 0;
  for (; $last_part < $parts_count; ++$last_part) {
    $length += strlen($parts[$last_part]);
    if ($length > $your_desired_width) { break; }
  }

  return implode(array_slice($parts, 0, $last_part));
}

/**
 * Helper to determine if the user has subscribed to the given training.
 *
 * @param $training
 *   Node to check subscription to.
 *
 * @return bool
 */
function _training_is_user_subscribed($training) {
  $subscribed = FALSE;
  switch ($training->training_type) {
    case '360learning':
      $user_id_360_learning = learning360_get_user_id();
      if ($user_id_360_learning) {
        // A user can either be registered in a group or to a session.
        $_360_training_id_field = field_get_items('node', $training, LEARNING360_ID_FIELD);
        $_360_training_group_id_field = field_get_items('node', $training, LEARNING360_GROUP_FIELD);
        if (!empty($_360_training_group_id_field) and count($_360_training_group_id_field) > 0) {
          $subscribed = learning360_user_has_group($_360_training_group_id_field[0]['value'], $user_id_360_learning);
        }
        if (!isset($subscribed) or !$subscribed) {
          $subscribed = learning360_user_has_session($_360_training_id_field[0]['value'], $user_id_360_learning);
        }
      }
      break;
    case 'yes_wiki':
    case 'external':
      $subscribed = _training_is_user_subscribed_to_training_node($training->nid);
      break;
    default:
      throw new Exception('Unsupported training type ' . $training->training_type . ' to check if the user is subscribed to or not.');
      break;
  }
  return $subscribed;
}

/**
 * Helper to determine if the user is subscribed to the training.
 *
 * @param $training_nid
 *   Node ID.
 *
 * @return bool
 */
function _training_is_user_subscribed_to_training_node($training_nid) {
  $user = user_load($GLOBALS['user']->uid);
  if (!empty($user->field_training_sessions) && count($user->field_training_sessions[LANGUAGE_NONE]) > 0) {
    // If the formatter is colibris_universite_user_trainings_list the data
    // structure is different (see prepare method).
    if (isset($user->field_training_sessions[LANGUAGE_NONE]['subscribed'])) {
      foreach ($user->field_training_sessions[LANGUAGE_NONE]['subscribed'] as $delta => $sub_training_nid) {
        if ($sub_training_nid == $training_nid) {
          return TRUE;
        }
      }
    }
    else {
      foreach ($user->field_training_sessions[LANGUAGE_NONE] as $delta => $training) {
        if ($training['target_id'] == $training_nid) {
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function colibris_universite_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  // Only admin can edit a user training sessions directly.
  if (!in_array('Admin', $GLOBALS['user']->roles)) {
    $form['field_training_sessions']['#access'] = FALSE;
  }
}

/**
 * Helper to determine if a node field is empty or not.
 *
 * @param $field_data
 *   Node field content.
 *
 * @return bool
 */
function _field_has_content($field_data) {
  return (isset($field_data[LANGUAGE_NONE]) && !empty($field_data[LANGUAGE_NONE]) && count($field_data[LANGUAGE_NONE]) > 0);
}

/**
 * Implements hook_field_formatter_info().
 */
function colibris_universite_field_formatter_info() {
  return [
    'colibris_universite_user_trainings_list' => [
      'label' => t('User trainings list'),
      'field types' => ['entityreference'],
    ],
  ];
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function colibris_universite_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  if ($entity_type == 'user') {
    foreach ($displays as $user_id => $user_entity_view_info) {
      // Build two list of trainings. One with the ongoing training, another
      // one with the full list of subscribed trainings.
      if ($user_entity_view_info['type'] == 'colibris_universite_user_trainings_list') {
        $training_nids = [];
        // Extract trainings from data.
        foreach ($items[$user_id] as $user_training) {
          $training_nids[] = $user_training['target_id'];
        }
        $training_nodes = node_load_multiple($training_nids);
        $registered_trainings = [];
        $primary_trainings = [];
        foreach ($training_nodes as $training_node) {
          // If training on going.
          if (in_array($training_node->training_access_status, [TRAINING_REGISTRABLE, TRAINING_ALWAYS_ACCESSIBLE])) {
            $primary_trainings[$training_node->created] = $training_node->nid;
          }
          $registered_trainings[$training_node->created] = $training_node->nid;
        }
        // We will sort the trainings on the creation date DESC.
        krsort($primary_trainings);
        krsort($registered_trainings);
        $items[$user_id] = [
          'ongoing' => $primary_trainings,
          'subscribed' => $registered_trainings,
        ];
      }
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function colibris_universite_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = [];
  if ($display['type'] == 'colibris_universite_user_trainings_list') {
    $element = [
      '#attached' => [
        'library' => [['system', 'ui.tabs']],
      ],
      '#theme' => 'colibris_universite_user_trainings_list',
      '#ongoing' => entity_view('node', node_load_multiple($items['ongoing']), 'teaser'),
      '#subscribed' => entity_view('node', node_load_multiple($items['subscribed']), 'teaser'),
    ];
  }

  return $element;
}

/**
 * Implements hook_theme().
 */
function colibris_universite_theme($existing, $type, $theme, $path) {
  return [
    'colibris_universite_user_trainings_list' => [
      'template' => 'templates/user-trainings-list',
      'variables' => ['ongoing' => [], 'subscribed' => []],
    ],
  ];
}
