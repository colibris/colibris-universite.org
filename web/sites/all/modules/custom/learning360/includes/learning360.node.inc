<?php
/**
 * Redirect an anonymous user to register page.
 *
 * @param bool $ajax AJAX query.
 */
function learning360_node_register_redirect($ajax = FALSE) {
  global $user;
  if (empty($user->uid)) {
    if ($ajax) {
      drupal_json_output(array('error' => 'authentification required'));
      drupal_exit();
    } else {
      drupal_goto('user/register', [
        'query' => [
          'destination' => current_path(),
        ],
      ]);
    }
  }
}

/**
 * Subscribe to a session.
 *
 * @param object $node Session node.
 * @param bool $ajax AJAX query.
 */
function learning360_node_subscribe($node, $ajax = FALSE) {
  learning360_node_register_redirect($ajax);

  $user_id = learning360_get_user_id();
  $field_id = field_get_items('node', $node, LEARNING360_ID_FIELD);
  $session_id = $field_id[0]['value'];

  $response = learning360_call('addUserToSession', array(
    'user_id' => $user_id,
    'session_id' => $session_id,
  ));

  if ($ajax) {
    if (FALSE === $response || !empty($response['error'])) {
      drupal_json_output(array('error' => !empty($response['error']) ? $response['error'] :'service failed'));
    } else {
      drupal_json_output(array('status' => 'success'));
    }
    drupal_exit();
  } else {
    if (FALSE === $response) {
      // drupal_set_message(t('An error occured while subscribing. Please try again later or contact an administrator.'), 'error');
    } elseif (!empty($response['error'])) {
      //drupal_set_message(t('An error occured while subscribing: @message.', array('@message' => $response['error'],)), 'warning');
      watchdog('learning360', t('An error occured while subscribing: @message.', array('@message' => $response['error'],)), WATCHDOG_NOTICE);
    } else {
      //drupal_set_message(t('You subscribed to "@title".', array('@title' => $node->title,)));
    }

    $node_uri = entity_uri('node', $node);
    drupal_goto($node_uri['path'], $node_uri['options'], 302);
  }
}

/**
 * Unsubscribe to a session.
 *
 * @param object $node Session node.
 * @param bool $ajax AJAX query.
 */
function learning360_node_unsubscribe($node, $ajax = FALSE) {
  learning360_node_register_redirect();

  $user_id = learning360_get_user_id();
  $field_id = field_get_items('node', $node, LEARNING360_ID_FIELD);
  $session_id = $field_id[0]['value'];

  $response = learning360_call('removeUserFromSession', array(
    'user_id' => $user_id,
    'session_id' => $session_id,
  ));

  if ($ajax) {
    if (FALSE === $response || !empty($response['error'])) {
      drupal_json_output(array('error' => !empty($response['error']) ? $response['error'] :'service failed'));
    } else {
      drupal_json_output(array('status' => 'success'));
    }
    drupal_exit();
  } else {
    if (FALSE === $response) {
      //drupal_set_message(t('An error occured while unsubscribing. Please try again later or contact an administrator.'), 'error');
    } elseif (!empty($response['error'])) {
      //drupal_set_message(t('An error occured while unsubscribing: @message.', array('@message' => $response['error'],)), 'warning');
      watchdog('learning360', t('An error occured while unsubscribing: @message.', array('@message' => $response['error'],)), WATCHDOG_NOTICE);
    } else {
      //drupal_set_message(t('You unsubscribed from "@title".', array('@title' => $node->title,)));
    }

    $node_uri = entity_uri('node', $node);
    drupal_goto($node_uri['path'], $node_uri['options'], 302);
  }
}

/**
 * Subscribe to a group.
 *
 * @param object $node group node.
 * @param bool $ajax AJAX query.
 */
function learning360_node_subscribe_to_group($node, $ajax = FALSE) {
  learning360_node_register_redirect($ajax);

  global $user;

  $field_id = field_get_items('node', $node, LEARNING360_GROUP_FIELD);
  $group_id = $field_id[0]['value'];

  $response = learning360_call('addUserToGroup', array(
    'mail' => $user->mail,
    'group_id' => $group_id,
  ));

  if ($ajax) {
    if (FALSE === $response || !empty($response['error'])) {
      drupal_json_output(array('error' => !empty($response['error']) ? $response['error'] :'service failed'));
    } else {
      drupal_json_output(array('status' => 'success'));
    }
    drupal_exit();
  } else {
    if (FALSE === $response) {
      // drupal_set_message(t('An error occured while subscribing to group. Please try again later or contact an administrator.'), 'error');
    } elseif (!empty($response['error'])) {
      //drupal_set_message(t('An error occured while subscribing to group : @message.', array('@message' => $response['error'],)), 'warning');
      watchdog('learning360', t('An error occured while subscribing to group : @message.', array('@message' => $response['error'],)), WATCHDOG_NOTICE);
    } else {
      //drupal_set_message(t('You subscribed to "@title".', array('@title' => $node->title,)));
    }

    $node_uri = entity_uri('node', $node);
    drupal_goto($node_uri['path'], $node_uri['options'], 302);
  }
}

/**
 * Unsubscribe to a session.
 *
 * @param object $node Session node.
 * @param bool $ajax AJAX query.
 */
function learning360_node_unsubscribe_to_group($node, $ajax = FALSE) {
  learning360_node_register_redirect();

  global $user;

  $field_id = field_get_items('node', $node, LEARNING360_GROUP_FIELD);
  $group_id = $field_id[0]['value'];

  $response = learning360_call('deleteUserFromGroup', array(
    'mail' => $user->mail,
    'group_id' => $group_id,
  ));

  if ($ajax) {
    if (FALSE === $response || !empty($response['error'])) {
      drupal_json_output(array('error' => !empty($response['error']) ? $response['error'] :'service failed'));
    } else {
      drupal_json_output(array('status' => 'success'));
    }
    drupal_exit();
  } else {
    if (FALSE === $response) {
      //drupal_set_message(t('An error occured while unsubscribing from group. Please try again later or contact an administrator.'), 'error');
    } elseif (!empty($response['error'])) {
      //drupal_set_message(t('An error occured while unsubscribing from group : @message.', array('@message' => $response['error'],)), 'warning');
      watchdog('learning360', t('An error occured while unsubscribing from group : @message.', array('@message' => $response['error'],)), WATCHDOG_NOTICE);
    } else {
      //drupal_set_message(t('You unsubscribed from "@title".', array('@title' => $node->title,)));
    }

    $node_uri = entity_uri('node', $node);
    drupal_goto($node_uri['path'], $node_uri['options'], 302);
  }
}
