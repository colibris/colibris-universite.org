<?php
/**
 * Helper definition of the id field base.
 */
function learning360_id_field_base() {
  return array(
    'field_name' => LEARNING360_ID_FIELD,
    'type' => 'text',
    'module' => 'text',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
      'max_length' => 32,
    ),
  );
}

/**
 * Helper definition of the id field instance.
 */
function learning360_id_field_instance() {
  return array(
    'label' => '360Learning ID',
    'field_name' => LEARNING360_ID_FIELD,
    'default_value' => NULL,
    'settings' => array(
      'text_processing' => FALSE,
    ),
    'widget' => array(
      'type' => 'text_textfield',
    ),
    'display' => array(),
  );
}

/**
 * Helper definition of the id field instance.
 */
function learning360_id_field_create($entity_type, $bundle) {
  $id_instance = learning360_id_field_instance();
  $id_instance['entity_type'] = $entity_type;
  $id_instance['bundle'] = $bundle;

  $existing_instance = field_read_instance($id_instance['entity_type'], $id_instance['field_name'], $id_instance['bundle']);
  if (empty($existing_instance)) {
    field_create_instance($id_instance);
  }
}
