<?php
/**
 * Implements hook_form().
 */
function learning360_settings_form($form = array(), &$form_state) {
  // Connection
  $form['learning360_connect'] = array(
    '#title' => t('Connection'),
    '#type' => 'fieldset',
  );

  $form['learning360_connect']['learning360_host'] = array(
    '#title' => t('Host'),
    '#description' => t('Endpoint host with no trailing slash. Example: https://mydomain.360learning.com'),
    '#type' => 'textfield',
    '#default_value' => variable_get('learning360_host'),
    '#element_validate' => array('learning360_validate_host'),
    '#required' => TRUE,
  );

  $form['learning360_connect']['learning360_company'] = array(
    '#title' => t('Company'),
    '#type' => 'textfield',
    '#default_value' => variable_get('learning360_company'),
    '#element_validate' => array('learning360_validate_company'),
    '#required' => TRUE,
  );

  $form['learning360_connect']['learning360_apikey'] = array(
    '#title' => t('API key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('learning360_apikey'),
    '#element_validate' => array('learning360_validate_apikey'),
    '#required' => TRUE,
  );

  // Fields mapping
  $user_fields = field_read_fields(array(
    'entity_type' => 'user',
    'bundle' => 'user',
  ));
  $user_field_instances = field_info_instances('user', 'user');

  $user_text_fields = array();
  foreach($user_fields as $user_fieldname => $user_field) {
    switch($user_field['type']) {
      case 'text':
      case 'text_long':
        $user_text_fields[$user_fieldname] = $user_field_instances[$user_fieldname]['label'];
        break;
    }
  }

  $user_field_mapping = variable_get('learning360_user_fields_mapping', array());
  $form['learning360_user_fields_mapping'] = array(
    '#title' => t('User fields mapping'),
    '#description' => t('Fields will be stored in the user data storage in any case.'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );

  $form['learning360_user_fields_mapping']['firstName'] = array(
    '#title' => t('First name'),
    '#type' => 'select',
    '#options' => $user_text_fields,
    '#empty_option' => '- ' . t('None') . ' -',
    '#default_value' => isset($user_field_mapping['firstName']) ? $user_field_mapping['firstName'] : NULL,
  );

  $form['learning360_user_fields_mapping']['lastName'] = array(
    '#title' => t('Last name'),
    '#type' => 'select',
    '#options' => $user_text_fields,
    '#empty_option' => '- ' . t('None') . ' -',
    '#default_value' => isset($user_field_mapping['lastName']) ? $user_field_mapping['lastName'] : NULL,
  );

  // Sessions
  $form['learning360_session'] = array(
    '#title' => t('Sessions'),
    '#type' => 'fieldset',
  );

  $session_ntype = variable_get('learning360_session_ntype');
  $form['learning360_session']['learning360_session_ntype'] = array(
    '#title' => t('Node type'),
    '#description' => t('Imported sessions will be based on selected node type. This will create automatically a field learning360_id.'),
    '#type' => 'select',
    '#options' => node_type_get_names(),
    '#default_value' => $session_ntype,
    '#required' => TRUE,
  );

  // Fields mapping
  if (!empty($session_ntype)) {
    $session_fields = field_read_fields([
      'entity_type' => 'node',
      'bundle' => $session_ntype,
    ]);
    $session_field_instances = field_info_instances('node', $session_ntype);

    $session_date_options = array();
    foreach ($session_fields as $session_fieldname => $session_field) {
      switch ($session_field['type']) {
        case 'date':
        case 'datestamp':
        case 'datetime':
          $session_date_options[$session_fieldname] = $session_field_instances[$session_fieldname]['label'];
          break;
      }
    }

    $session_field_mapping = variable_get('learning360_session_fields_mapping', array());
    $form['learning360_session_fields_mapping'] = array(
      '#title' => t('User fields mapping'),
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );

    $form['learning360_session_fields_mapping']['startDate'] = array(
      '#title' => t('Start date'),
      '#description' => t('If start date and end date fields are the same, values will be stored in value1 and value2 properties.'),
      '#type' => 'select',
      '#options' => $session_date_options,
      '#empty_option' => '- ' . t('None') . ' -',
      '#default_value' => isset($session_field_mapping['startDate']) ? $session_field_mapping['startDate'] : NULL,
    );

    $form['learning360_session_fields_mapping']['endDate'] = array(
      '#title' => t('End date'),
      '#description' => t('If start date and end date fields are the same, values will be stored in value1 and value2 properties.'),
      '#type' => 'select',
      '#options' => $session_date_options,
      '#empty_option' => '- ' . t('None') . ' -',
      '#default_value' => isset($session_field_mapping['endDate']) ? $session_field_mapping['endDate'] : NULL,
    );
  }

  $form['#validate'][] = 'learning360_validate_fields_mapping';
  $form['#submit'][] = 'learning360_submit_id_field';

  return system_settings_form($form);
}

/**
 * Implements hook_form().
 */
function learning360_user_form($form = array(), &$form_state, $user) {
  form_load_include($form_state, 'inc', 'learning360', 'includes/learning360.crud');

  $form['actions'] = array(
    '#type' => 'actions'
  );

  if (!empty($user->data['learning360__id'])) {
    foreach($user->data as $key => $data) {
      if (0 !== strpos($key, 'learning360_')) {
        continue;
      }

      $key_clean = str_replace('learning360_', '', $key);

      // Formattingg
      switch($key_clean) {
        case 'updated':
          $data = format_date($data, 'short');
          break;
      }

      $form[$key_clean] = array(
        '#title' => $key_clean,
        '#title_display' => 'before',
        '#type' => 'item',
        '#markup' => (is_array($data) ? print_r($data, TRUE) : $data),
      );
    }

    $form['actions']['update'] = array(
      '#value' => t('Update 360Learning informations'),
      '#type' => 'submit',
      '#submit' => array('learning360_user_update_submit')
    );
    $form['actions']['remove'] = array(
      '#value' => t('Unbind 360Learning account'),
      '#type' => 'submit',
      '#submit' => array('learning360_user_remove_submit')
    );
  } else {
    $form['none'] = array(
      '#markup' => '<div class="messages warning">' . t('No 360Learning account is associated to this user.') . '</div>',
    );

    $form['actions']['submit'] = array(
      '#value' => t('Create or bind a 360Learning account'),
      '#type' => 'submit',
      '#submit' => array('learning360_user_create_submit'),
    );
  }

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function learning360_user_create_submit($form, &$form_state) {
  $user = $form_state['build_info']['args'][0];

  $status = learning360_user_create_profile($user);

  if ($status) {
    drupal_set_message(t('360Learning profile created successfully.'));
    user_save($user);
  } else {
    drupal_set_message(t('An error occured during the 360Learning profile creation, try again later or contact an administrator.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function learning360_user_update_submit($form, &$form_state) {
  $user = $form_state['build_info']['args'][0];

  $status = learning360_user_update_profile($user);

  if ($status) {
    drupal_set_message(t('360Learning profile updated successfully.'));
    user_save($user);
  } else {
    drupal_set_message(t('An error occured during the 360Learning profile update, try again later or contact an administrator.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function learning360_user_remove_submit($form, &$form_state) {
  $user = $form_state['build_info']['args'][0];

  $status = learning360_user_remove_profile($user);

  if ($status) {
    drupal_set_message(t('360Learning profile unbinded successfully.'));
    user_save($user);
  } else {
    drupal_set_message(t('An error occured during the 360Learning profile unbind, try again later or contact an administrator.'));
  }
}

/**
 * Implements hook_element_validate().
 */
function learning360_validate_host($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '') {
    // Remove trailing slash
    if ('/' === substr($value, -1)) {
      $value = rtrim($value, '/');
      form_set_value($element, $value, $form_state);
    }

    // Ensure protocol
    if (0 !== strpos($value, 'https://') && 0 !== strpos($value, 'http://')) {
      form_error($element, t('%name must start with a protocol (example: https://).', array(
        '%name' => $element['#title'],
      )));
    }
  }
}

/**
 * Implements hook_element_validate().
 */
function learning360_validate_company($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && !preg_match('/[a-z\d]{24}/', $value)) {
    form_error($element, t('%name must be a valid key (24 letters or numbers).', array(
      '%name' => $element['#title'],
    )));
  }
}

/**
 * Implements hook_element_validate().
 */
function learning360_validate_apikey($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && !preg_match('/[a-z\d]{32}/', $value)) {
    form_error($element, t('%name must be a valid key (32 letters or numbers).', array(
      '%name' => $element['#title'],
    )));
  }
}

/**
 * Implements hook_form_validate().
 * Remove empty values.
 */
function learning360_validate_fields_mapping($form, &$form_state) {
  if (isset($form_state['values']['learning360_user_fields_mapping'])) {
    $form_state['values']['learning360_user_fields_mapping'] = array_filter($form_state['values']['learning360_user_fields_mapping']);
  }
}

/**
 * Implements hook_form_submit().
 * Create ID fields.
 */
function learning360_submit_id_field($form, &$form_state) {
  $ntype = $form_state['values']['learning360_session_ntype'];
  $old_ntype = variable_get('learning360_session_ntype');

  if (!empty($ntype) && $ntype !== $old_ntype) {
    if (module_load_include('inc', 'learning360', 'includes/learning360.db')) {
      learning360_id_field_create('node', $ntype);
    }
  }
}