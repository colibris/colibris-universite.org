<?php
/**
 * Build API endpoint.
 *
 * @return string 360Learning API endpoint.
 */
function learning360_service_endpoint() {
  $endpoint = &drupal_static(__FUNCTION__, NULL);

  if (!isset($endpoint)) {
    $endpoint = variable_get('learning360_host') . '/api/v1';
  }

  return $endpoint;
}

/**
 * Check the query requirements.
 *
 * @param string $method GET, POST, PUT or DEL
 * @param string $path Path from endpoint
 * @param array $args Arguments to pass
 *
 * @return mixed Response array or FALSE.
 */
function learning360_check_query_requirements() {
  $checked = &drupal_static(__FUNCTION__, NULL);

  if (!isset($checked)) {
    $checked = !empty(variable_get('learning360_host'))
      && !empty(variable_get('learning360_company'))
      && !empty(variable_get('learning360_apikey'));
  }

  return $checked;
}

/**
 * Verify that the required options are presents.
 *
 * @param string $service Name of the service to call.
 * @param array $options Options passed to the service.
 *
 * @return boolean Requirement status.
 */
function learning360_check_service_requirements($service, $options) {
  $checked = TRUE;
  $requirements = array();

  switch($service) {
    // Users
    case 'getUser':
      $requirements = array('mail');
      break;

    case 'createUser':
      $requirements = array('mail', 'password');
      break;

    case 'updateUser':
      $requirements = array('mail', 'mail_new');
      break;

    case 'deleteUser':
      $requirements = array('mail');
      break;

    case 'getUserPrograms':
      $requirements = array('user_id');
      break;

    // Sessions
    case 'listSessions':
      $requirements = array();
      break;

    case 'getProgramSessionUsers':
      $requirements = array('session_id');
      break;

    case 'addUserToSession':
      $requirements = array('session_id', 'user_id');
      break;

    case 'removeUserFromSession':
      $requirements = array('session_id', 'user_id');
      break;

    // Groups
    case 'getGroups':
      $requirements = array();
      break;

    case 'getGroup':
      $requirements = array('group_id');
      break;

    case 'addUserToGroup':
      $requirements = array('group_id', 'mail');
      break;

    case 'deleteUserFromGroup':
      $requirements = array('group_id', 'mail');
      break;

    default:
      // Unsupported service
      $checked = FALSE;
  }

  foreach($requirements as $option_key) {
    if (!isset($options[$option_key])) {
      $checked = FALSE;
      break;
    }
  }

  return $checked;
}

/**
 * Build arguments passed to learning360_query().
 *
 * @param string $service Name of the service to call.
 * @param array $options Options passed to the service.
 *
 * @return array method, path and arguments.
 */
function learning360_build_arguments($service, $options) {
  // Defaults
  $method = 'GET';
  $path = '';
  $args = array();

  // Prepare the arguments
  switch($service) {
    // Users
    case 'getUser':
      $path = '/users/' . urlencode(strtolower($options['mail']));
      break;

    case 'createUser':
      global $language;
      $method = 'POST';
      $path = '/users';
      $args = $options;
      $args['role'] = 'learner';
      $args['lang'] = $language->language;
      break;

    case 'updateUser':
      $method = 'PUT';
      $path = '/users/' . urlencode(strtolower($options['mail']));
      $args = array('mail' => strtolower($options['mail_new']));
      if (!empty($options['password'])) {
        $args['password'] = $options['password'];
      }
      break;

    case 'deleteUser':
      $method = 'DELETE';
      $path = '/users/' . urlencode(strtolower($options['mail']));
      break;

    case 'getUserPrograms':
      $path = '/users/' . urlencode($options['user_id']) . '/programs';
      break;

    // Sessions
    case 'listSessions':
      $path = '/programs/sessions';
      break;

    case 'getProgramSessionUsers':
      $path = '/programs/sessions/' . $options['session_id'] . '/users';
      break;

    case 'addUserToSession':
      $method = 'PUT';
      $path = '/programs/sessions/' . $options['session_id'] . '/users/' . urlencode($options['user_id']);
      break;

    case 'removeUserFromSession':
      $method = 'DELETE';
      $path = '/programs/sessions/' . $options['session_id'] . '/users/' . urlencode($options['user_id']);
      break;

    // Groups
    case 'getGroups':
      $path = '/groups';
      break;

    case 'getGroup':
      $path = '/groups/' . $options['group_id'];
      break;

    case 'addUserToGroup':
      $method = 'PUT';
      $path = '/groups/' . $options['group_id'] . '/users/' . urlencode(strtolower($options['mail']));
      break;

    case 'deleteUserFromGroup':
      $method = 'DELETE';
      $path = '/groups/' . $options['group_id'] . '/users/' . urlencode(strtolower($options['mail']));
      break;
  }

  return array(
    $method,
    $path,
    $args,
  );
}

/**
 * Build API query.
 *
 * @param string $method GET, POST, PUT or DEL
 * @param string $path Path from endpoint
 * @param array $args Arguments to pass
 *
 * @return resource cURL handler.
 */
function learning360_build_query($method = 'GET', $path = '', array $args) {
  $method = strtoupper($method);

  // Add API keys
  $args['company'] = variable_get('learning360_company');
  $args['apiKey'] = variable_get('learning360_apikey');
  $query_args = http_build_query($args);

  // Build URL
  $url = learning360_service_endpoint() . $path;
  if ('POST' !== $method) {
    $url .= '?' . $query_args;
  }

  // cURL handler
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  // Method specifics
  switch($method) {
    case 'POST':
      curl_setopt($ch, CURLOPT_POST, TRUE);
      break;
    case 'PUT':
      curl_setopt($ch, CURLOPT_PUT, TRUE);
      break;
    case 'DELETE':
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
      break;
  }

  // Arguments
  if ('POST' === $method) {
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query_args);
  }

  return $ch;
}

/**
 * Query API endpoint.
 *
 * @param string $method GET, POST, PUT or DEL.
 * @param string $path Path from endpoint.
 * @param array $args Arguments to pass.
 *
 * @return mixed Response array or FALSE.
 */
function learning360_exec_query($method = 'GET', $path = '', array $args) {
  $ch = learning360_build_query($method, $path, $args);

  $response = curl_exec($ch);
  if (FALSE === $response) {
    watchdog(
      'learning360',
      'An error occured during the API Call @url: @message.',
      array('@url' => curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), '@message' => curl_error($ch)),
      WATCHDOG_ERROR
    );
  } else {
    $response = !empty($response) ? drupal_json_decode($response) : NULL;
  }
  curl_close($ch);

  return $response;
}

/**
 * Execute post query actions if necessary.
 *
 * @param string $service Name of the service to call.
 * @param array $options Options passed to the service.
 * @param array $response Service response.
 */
function learning360_post_query($service, $options, $response) {
  switch($service) {
    case 'addUserToSession':
      if (module_load_include('inc', 'learning360', 'includes/learning360.crud')) {
        learning360_session_add($options['session_id'], $options['user_id']);
      }
      break;

    case 'removeUserFromSession':
      if (module_load_include('inc', 'learning360', 'includes/learning360.crud')) {
        learning360_session_delete($options['session_id'], $options['user_id']);
      }
      break;

    case 'addUserToGroup':
      if (module_load_include('inc', 'learning360', 'includes/learning360.crud')) {
        $user = user_load_by_mail($options['mail']);
        $user_id = !empty($user) ? learning360_get_user_id($user) : NULL;

        if (!empty($user_id)) {
          learning360_group_add($options['group_id'], $user_id);
          //  // delete from main group
          //  $response = learning360_call('deleteUserFromGroup', array(
          //   'mail' => $options['mail'],
          //   'group_id' => '5694ec660fa69fdd0ec017d3',
          // ));
          // learning360_group_delete('5694ec660fa69fdd0ec017d3', $user_id);
        }
      }
      break;

    case 'deleteUserFromGroup':
      if (module_load_include('inc', 'learning360', 'includes/learning360.crud')) {
        $user = user_load_by_mail($options['mail']);
        $user_id = !empty($user) ? learning360_get_user_id($user) : NULL;

        if (!empty($user_id)) {
          learning360_group_delete($options['group_id'], $user_id);
        }
      }
      break;
  }
}

/**
 * Call 360Learning services.
 *
 * @param string $service Name of the service to call.
 * @param array $options Options passed to the service.
 *
 * @return mixed Service return.
 */
function learning360_query($service, $options) {
  $response = FALSE;
  if (!learning360_check_query_requirements()) {
    return $response;
  }

  // Requirements checks
  if (!learning360_check_service_requirements($service, $options)) {
    return $response;
  }

  // Prepare the arguments
  list($method, $path, $args) = learning360_build_arguments($service, $options);

  // Query the service
  $response = learning360_exec_query($method, $path, $args);

  // Execute post-query actions
  if (FALSE !== $response && empty($response['error'])) {
    learning360_post_query($service, $options, $response);
  }

  return $response;
}

/**
 * Query API endpoint.
 *
 * @param resource $mh cURL multi handler.
 * @param array $chs cURL handlers.
 *
 * @return array Response.
 */
function learning360_multi_exec_query($mh, array $chs) {
  $responses = array();

  // Execute
  $running = NULL;
  do {
    curl_multi_exec($mh, $running);
    curl_multi_select($mh);
  } while ($running > 0);

  // Process the results
  foreach ($chs as $id => $ch) {
    $response = curl_multi_getcontent($ch);

    if (FALSE !== $response) {
      $response = !empty($response) ? drupal_json_decode($response) : NULL;
      $responses[$id] = $response;
    }

    curl_multi_remove_handle($mh, $ch);
  }

  // Close
  curl_multi_close($mh);

  return $responses;
}

/**
 * Call multiple 360Learning services.
 *
 * @param array $queries Array of service and options.
 *
 * @return mixed Service return.
 */
function learning360_multi_query($queries) {
  $responses = array();
  if (!learning360_check_query_requirements()) {
    return $responses;
  }

  $mh = curl_multi_init();
  $chs = array();

  foreach($queries as $id => $query) {
    // Invalid parameters
    if (empty($query['service'])) {
      continue;
    }

    $service = $query['service'];
    $options = !empty($query['options']) ? $query['options'] : array();

    // Requirements checks
    if (!learning360_check_service_requirements($service, $options)) {
      continue;
    }

    // Prepare the arguments
    list($method, $path, $args) = learning360_build_arguments($service, $options);

    $chs[$id] = learning360_build_query($method, $path, $args);

    curl_multi_add_handle($mh, $chs[$id]);
  }

  // Query the service
  $responses = learning360_multi_exec_query($mh, $chs);

  return $responses;
}
