<?php
/**
 * Create user profile.
 *
 * @param mixed $account Array or user object.
 *
 * @return mixed Array or user object.
 */
function learning360_user_create_profile(&$account) {
  $is_array = is_array($account);
  $account = $is_array ? $account : (array) $account;

  // Prepare account data
  $account_data = array(
    'mail' => strtolower($account['mail']),
    'password' => !empty($account['learning360_pass']) ? $account['learning360_pass'] : user_password(),
  );

  // Map fields
  $field_mapping = variable_get('learning360_user_fields_mapping', array());
  foreach($field_mapping as $field => $local_field) {
    if (!empty($account[$local_field][LANGUAGE_NONE][0]['value'])) {
      $account_data[$field] = $account[$local_field][LANGUAGE_NONE][0]['value'];
    }
  }

  // Create user
  $create_user = learning360_call('createUser', $account_data);

  // Account creation failed
  if (!empty($create_user['error']) && 'user_already_exists' !== $create_user['error']) {
    watchdog('learning360', t('An error occured while creating profile: @message.', array('@message' => $create_user['error'],)), WATCHDOG_NOTICE);
    return FALSE;
  }

  // Gather some infos about the user
  learning360_user_update_profile($account);

  $account = $is_array ? $account : (object) $account;
  return TRUE;
}

/**
 * Update user profile.
 *
 * @param mixed $account Array or user object.
 *
 * @return mixed Array or user object.
 */
function learning360_user_update_profile(&$account) {
  $is_array = is_array($account);
  $account = $is_array ? $account : (array) $account;

  // Gather some info about the user
  $get_user = learning360_call('getUser', array(
    'mail' => strtolower($account['mail']),
  ));

  // Update info
  if (!empty($get_user['_id'])) {
    $account[LEARNING360_ID_FIELD][LANGUAGE_NONE][0]['value'] = $get_user['_id'];

    $field_mapping = variable_get('learning360_user_fields_mapping', array());
    foreach($get_user as $field_key => $field_value) {
      $account['data']['learning360_' . $field_key] = $field_value;
      if (isset($field_mapping[$field_key]) && isset($account[$field_mapping[$field_key]]) && empty($account[$field_mapping[$field_key]][LANGUAGE_NONE][0]['value'])) {
        $account[$field_mapping[$field_key]][LANGUAGE_NONE][0]['value'] = $field_value;
      }
    }

    $account['data']['learning360_updated'] = REQUEST_TIME;
  } else {
    return FALSE;
  }

  // Groups
  if (isset($get_user['groups'])) {
    $group_ids = array();
    foreach($get_user['groups'] as $group) {
      $group_ids[] = $group['_id'];
    }
    learning360_groups_update($group_ids, $get_user['_id']);
  }

  $account = $is_array ? $account : (object) $account;

  // Update sessions associations
  $get_programs = learning360_call('getUserPrograms', array(
    'user_id' => $get_user['_id'],
  ));

  if (FALSE !== $get_programs && empty($get_programs['error'])) {
    $programs = array();
    foreach($get_programs as $program) {
      $programs[] = $program['_id'];
    }
    learning360_sessions_update($programs, $get_user['_id']);
  }

  return TRUE;
}

/**
 * Update user email.
 *
 * @param mixed $account Array or user object.
 * @param string $mail_old Old email address.
 *
 * @return mixed Array or user object.
 */
function learning360_user_update_mail(&$account, $mail_old) {
  $is_array = is_array($account);
  $account = $is_array ? $account : (array) $account;

  $update_email = learning360_call('updateUser', array(
    'mail' => $mail_old,
    'mail_new' => $account['mail'],
  ));

  if (!empty($update_email['error'])) {
    watchdog('learning360', t('An error occured while updating profile: @message.', array('@message' => $update_email['error'],)), WATCHDOG_NOTICE);
    return FALSE;
  }

  // Update profile infos
  learning360_user_update_profile($account);

  $account = $is_array ? $account : (object) $account;

  return TRUE;
}

/**
 * Update user email.
 *
 * @param mixed $account Array or user object.
 * @param string $mail_old Old email address.
 *
 * @return mixed Array or user object.
 */
function learning360_user_remove_profile(&$account) {
  $is_array = is_array($account);
  $account = $is_array ? $account : (array) $account;

  // ID field and sessions
  if (!empty($account[LEARNING360_ID_FIELD][LANGUAGE_NONE][0]['value'])) {
    learning360_sessions_update(array(), $account[LEARNING360_ID_FIELD][LANGUAGE_NONE][0]['value']);

    $account[LEARNING360_ID_FIELD][LANGUAGE_NONE][0]['value'] = '';
  }

  // Data storage
  if (!empty($account['data'])) {
    foreach(array_keys($account['data']) as $key) {
      if (0 !== strpos($key, 'learning360_')) {
        continue;
      } else {
        unset($account['data'][$key]);
      }
    }
  }

  $account = $is_array ? $account : (object) $account;

  return TRUE;
}

/**
 * Associate a session to a user.
 *
 * @param string $session_id Sesssion ID.
 * @param string $user_id User ID.
 *
 * @return bool Transaction status.
 */
function learning360_session_add($session_id, $user_id = NULL) {
  $user_id = !isset($user_id) ? learning360_get_user_id() : $user_id;

  if (!learning360_valid_id($session_id) || !learning360_valid_id($user_id)) {
    return FALSE;
  }

  // Test existing entry
  $existing = db_select('learning360_user_session', 'us')
    ->fields('us', array('uid'))
    ->condition('us.uid', $user_id)
    ->condition('us.sid', $session_id)
    ->execute()
    ->rowCount();

  if (!empty($existing)) {
    return TRUE;
  }

  // Insert new row
  db_insert('learning360_user_session')
    ->fields(array(
      'uid' => $user_id,
      'sid' => $session_id,
    ))
    ->execute();

  return TRUE;
}

/**
 * Remove a session from a user.
 *
 * @param string $session_id Sesssion ID.
 * @param string $user_id User ID.
 *
 * @return bool Transaction status.
 */
function learning360_session_delete($session_id, $user_id = NULL) {
  $user_id = !isset($user_id) ? learning360_get_user_id() : $user_id;

  if (!learning360_valid_id($session_id) || !learning360_valid_id($user_id)) {
    return FALSE;
  }

  // Remove entry if existing
  db_delete('learning360_user_session')
    ->condition('uid', $user_id)
    ->condition('sid', $session_id)
    ->execute();

  return TRUE;
}

/**
 * Update the user sessions with a new set of sessions.
 *
 * @param array $session_ids Session IDs.
 * @param string $user_id_360 360 learning User ID.
 * @param string $user_id_360 Drupal User ID.
 *
 * @return bool Transaction status.
 */
function learning360_sessions_update(array $session_ids, $user_id_360 = NULL, $user_id_drupal = NULL) {
  $user_id_360 = !isset($user_id_360) ? learning360_get_user_id() : $user_id_360;
  if (!learning360_valid_id($user_id_360)) {
    return FALSE;
  }
  $user_id_drupal = !isset($user_id_drupal) ? $GLOBALS['user']->uid : $user_id_drupal;
  if (!$user_id_drupal) {
    return FALSE;
  }
  $session_ids = array_filter($session_ids, 'learning360_valid_id');

  // Get training nids from their 360 learning ID.
  $training_nids = &drupal_static(__FUNCTION__, FALSE);
  if (!$training_nids) {
    $training_nids = array_flip(db_query("SELECT entity_id, learning360_id_value FROM {field_data_learning360_id} WHERE bundle = 'formation' AND learning360_id_value NOT LIKE 'http%'")->fetchAllKeyed(0));
  }

  // Test existing entry
  $existing_sessions = db_select('learning360_user_session', 'us')
    ->fields('us', array('sid'))
    ->condition('us.uid', $user_id_360)
    ->execute()
    ->fetchCol();

  $sessions_to_remove = !empty($existing_sessions) ? array_diff($existing_sessions, $session_ids) : array();
  $sessions_to_create = !empty($existing_sessions) ? array_diff($session_ids, $existing_sessions) : $session_ids;

  // Remove obsolete sessions
  if (!empty($sessions_to_remove)) {
    db_delete('learning360_user_session')
      ->condition('uid', $user_id_360)
      ->condition('sid', $sessions_to_remove, 'IN')
      ->execute();

    // Remove the associated training sessions from the user profile.
    foreach ($sessions_to_remove as $session_to_remove) {
      if (isset($training_nids[$session_to_remove])) {
        foreach (['data', 'revision'] as $key) {
          db_delete('field_' . $key . '_field_training_sessions')
            ->condition('entity_id', $user_id_360)
            ->condition('field_training_sessions_target_id', $training_nids[$session_to_remove])
            ->execute();
        }
      }
    }
  }

  // Insert new sessions
  if (!empty($sessions_to_create)) {
    $query = db_insert('learning360_user_session')
      ->fields(array('uid', 'sid'));
    foreach($sessions_to_create as $session_id) {
      $query->values(array(
        'uid' => $user_id_360,
        'sid' => $session_id,
      ));
    }
    $query->execute();

    // Add the session to user training field. Respect the delta structure.
    $delta = NULL;
    foreach ($sessions_to_create as $session_to_create) {
      if (isset($training_nids[$session_to_create])) {
        if ($delta == NULL) {
          $delta = db_query("SELECT MAX(delta) FROM {field_data_field_training_sessions} WHERE entity_id = $user_id_drupal")->fetchField(0);
          if (is_null($delta)) {
            $delta = -1;
          }
        }
        $delta++;
        db_query("INSERT INTO {field_data_field_training_sessions} VALUES('user', 'user', 0, $user_id_drupal, $user_id_drupal, 'und', $delta, $training_nids[$session_to_create])");
        db_query("INSERT INTO {field_revision_field_training_sessions} VALUES('user', 'user', 0, $user_id_drupal, $user_id_drupal, 'und', $delta, $training_nids[$session_to_create])");
      }
    }
  }
  return TRUE;
}

/**
 * Associate a group to a user.
 *
 * @param string $group_id Group ID.
 * @param string $user_id User ID.
 *
 * @return bool Transaction status.
 */
function learning360_group_add($group_id, $user_id = NULL) {
  $user_id = !isset($user_id) ? learning360_get_user_id() : $user_id;

  if (!learning360_valid_id($group_id) || !learning360_valid_id($user_id)) {
    return FALSE;
  }

  // Test existing entry
  $existing = db_select('learning360_user_group', 'ug')
    ->fields('ug', array('uid'))
    ->condition('ug.uid', $user_id)
    ->condition('ug.gid', $group_id)
    ->execute()
    ->rowCount();

  if (!empty($existing)) {
    return TRUE;
  }

  // Insert new row
  db_insert('learning360_user_group')
    ->fields(array(
      'uid' => $user_id,
      'gid' => $group_id,
    ))
    ->execute();

  return TRUE;
}

/**
 * Remove a user from a group.
 *
 * @param string $group_id group ID.
 * @param string $user_id User ID.
 *
 * @return bool Transaction status.
 */
function learning360_group_delete($group_id, $user_id = NULL) {
  $user_id = !isset($user_id) ? learning360_get_user_id() : $user_id;

  if (!learning360_valid_id($group_id) || !learning360_valid_id($user_id)) {
    return FALSE;
  }

  // Remove entry if existing
  db_delete('learning360_user_group')
    ->condition('uid', $user_id)
    ->condition('gid', $group_id)
    ->execute();

  return TRUE;
}

/**
 * Update the user groups with a new set of groups.
 *
 * @param array $group_ids Session IDs.
 * @param string $user_id User ID.
 *
 * @return bool Transaction status.
 */
function learning360_groups_update(array $group_ids, $user_id = NULL) {
  $user_id = !isset($user_id) ? learning360_get_user_id() : $user_id;

  if (!learning360_valid_id($user_id)) {
    return FALSE;
  }

  $group_ids = array_filter($group_ids, 'learning360_valid_id');

  // Test existing entry
  $existing_groups = db_select('learning360_user_group', 'ug')
    ->fields('ug', array('gid'))
    ->condition('ug.uid', $user_id)
    ->execute()
    ->fetchCol();

  $groups_to_remove = !empty($existing_groups) ? array_diff($existing_groups, $group_ids) : array();
  $groups_to_create = !empty($existing_groups) ? array_diff($group_ids, $existing_groups) : $group_ids;

  // Remove obsolete sessions
  if (!empty($groups_to_remove)) {
    db_delete('learning360_user_group')
      ->condition('uid', $user_id)
      ->condition('gid', $groups_to_remove, 'IN')
      ->execute();
  }

  // Insert new sessions
  if (!empty($groups_to_create)) {
    $query = db_insert('learning360_user_group')
      ->fields(array('uid', 'gid'));
    foreach($groups_to_create as $group_id) {
      $query->values(array(
        'uid' => $user_id,
        'gid' => $group_id,
      ));
    }
    $query->execute();
  }

  return TRUE;
}

/**
 * Update the user sessions.
 *
 * @param array $session_ids Session IDs.
 *
 * @return bool Transaction status.
 */
function learning360_programs_sessions_update(array $session_ids = array()) {
  $session_ntype = variable_get('learning360_session_ntype');
  if (empty($session_ntype)) {
    return FALSE;
  }

  // Gather all sessions
  $session_ids = empty($session_ids) ? array_keys(learning360_load_all_sessions_by_id()): $session_ids;

  // Prepare services queries
  $queries = array();
  foreach($session_ids as $session_id) {
    $queries[$session_id] = array(
      'service' => 'getProgramSessionUsers',
      'options' => array(
        'session_id' => $session_id,
      ),
    );
  }

  // Execute multi queries
  $responses = learning360_multi_call($queries);
  if (empty($responses)) {
    return FALSE;
  }

  $existing_users = learning360_load_all_users_by_id();

  // Process results
  $user_sessions = array();
  foreach($responses as $session_id => $response) {
    if (empty($response) || !empty($response['error'])) {
      continue;
    }

    foreach($response as $user_data) {
      if (!isset($user_data['_id'], $existing_users[$user_data['_id']])) {
        continue;
      }

      $user_sessions[$user_data['_id']][] = $session_id;
    }
  }

  // Process saving per user
  // @todo Review to lessen the DB queries.
  foreach($user_sessions as $user_id => $session_ids) {
    learning360_sessions_update($session_ids, $user_id, $existing_users[$user_id]);
  }

  return TRUE;
}

/**
 * Create or update program
 *
 * @param array $data Program data.
 * @param null $node Existing node.
 *
 * @return mixed Node ID or FALSE.
 */
function learning360_program_create(array $data = array(), $node = NULL) {
  if (empty($node)) {
    $node = new stdClass();
    $node->type = variable_get('learning360_session_ntype');
    $node->title = $data['name'];

    node_object_prepare($node);

    $node->status = NODE_NOT_PUBLISHED;
    $node->{LEARNING360_ID_FIELD}[LANGUAGE_NONE][0]['value'] = $data['_id'];
  }

  $node->title = $data['name'];

  // Fields mapping
  $session_field_mapping = variable_get('learning360_session_fields_mapping', array());

  // Start date
  if (isset($data['startDate']) && !empty($session_field_mapping['startDate'])) {
    $node->{$session_field_mapping['startDate']}[LANGUAGE_NONE][0]['value'] = !empty($data['startDate']) ?_learning360_format_date($data['startDate'], $session_field_mapping['startDate']) : NULL;
  }

  // End date
  if (isset($data['endDate']) && !empty($session_field_mapping['endDate'])) {
    $same_field = !empty($session_field_mapping['startDate']) && $session_field_mapping['startDate'] === $session_field_mapping['endDate'];
    $node->{$session_field_mapping['endDate']}[LANGUAGE_NONE][0][$same_field ? 'value2' : 'value'] = !empty($data['endDate']) ?_learning360_format_date($data['endDate'], $session_field_mapping['endDate']) : NULL;
  }

  node_save($node);

  return !empty($node->nid) ? $node->nid : FALSE;
}

/**
 * Update programs from the 360Learning service.
 *
 * @param bool $only_new Create only missing.
 *
 * @return mixed Array of ids or FALSE.
 */
function learning360_programs_update($only_new = TRUE) {
  // Require dedicated node type
  $node_type = variable_get('learning360_session_ntype');
  if (empty($node_type)) {
    return FALSE;
  }

  // Query the service
  $response = learning360_call('listSessions');
  if (FALSE === $response || !empty($response['error'])) {
    return FALSE;
  }

  // Format results
  $programs = $ids = array();
  foreach($response as $program) {
    $ids[] = $program['_id'];
    $programs[$program['_id']] = $program;
  }

  // Find existings
  $existings = learning360_load_sessions_by_id($ids, !$only_new, FALSE);
  if ($only_new) {
    foreach($existings as $_id => $nid) {
      unset($programs[$_id]);
    }
  }

  // Create new programs
  $created = array();
  if (!empty($programs)) {
    foreach($programs as $_id => $program) {
      $created_id = learning360_program_create($program, isset($existings[$_id]) ? $existings[$_id] : NULL);
      if (FALSE !== $created_id) {
        $created[] = $created_id;
      }
    }
  }

  return $created;
}

/**
 * Format date based on field setting.
 *
 * @param string $data Data.
 * @param string $field_name Field name
 *
 * @return false|int|null|string Formatted date.
 */
function _learning360_format_date($data = NULL, $field_name) {
  if (empty($field_name)) {
    return NULL;
  }

  $value = NULL;
  $field_settings = field_read_field($field_name);
  if (!empty($field_settings['field_name'])) {
    switch($field_settings['type']) {
      case 'date':
        $value = $data;
        break;

      case 'datestamp':
        $value = strtotime($data);
        break;

      case 'datetime':
        $value = date('Y-m-d H:i:s', strtotime($data));
        break;
    }
  }

  return $value;
}
