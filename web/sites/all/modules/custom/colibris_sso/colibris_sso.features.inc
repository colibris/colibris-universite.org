<?php

/**
 * @file
 * colibris_sso.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function colibris_sso_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
