<?php

/**
 *  Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function colibris_menu_local_tasks(&$vars) {
  $output = '';

  if (!empty($vars['primary'])) {
    $vars['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $vars['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $vars['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($vars['primary']);
  }

  if (!empty($vars['secondary'])) {
    $vars['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $vars['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $vars['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($vars['secondary']);
  }

  return $output;
}

function colibris_menu_link(array $vars) {
  $element = $vars['element'];
  $element['#attributes']['class'][] = 'menu-' . $element['#original_link']['mlid'];

  if(is_array($element['#attributes']['class'])) {
	  foreach ($element['#attributes']['class'] as $idx => $item) {
	   //   if (strpos($item, 'mlid') !== FALSE)
	   //   {
			 // unset($element['#attributes']['class'][$idx]);
	   //   }
	  }
  }
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function colibris_menu_contextual_links_alter(&$links, $router_item, $root_path) {
	if(isset($links['field_collection-edit'])) {
		$links = array_reverse($links);
	}
}

function colibris_form_search_api_page_search_form_search_alter(&$form, &$form_state, $form_id) {
	$form['keys_1']['#title']= t('keywords');
	$form['keys_1']['#attributes']['placeholder'] = t('keywords');
}

function colibris_form_alter(&$form, &$form_state, $form_id)
{
    $destination = isset($_GET['destination']) ? '?destination='.$_GET['destination'] : '';
    switch ($form_id) {
        case 'user_login':
            $form['#prefix'] = '<div class="grid-fit">
<div class="box">
<h1 class="page-name">connexion</h1>
<p>Une fois identifié, vous pourrez accéder à votre espace personnel, retrouver toutes vos formations, et vous inscrire à des nouvelles formations.</p>
</div>
<div class="box">';
            $form['name']['#title'] = t('Votre Email');
            $form['actions']['submit']['#value'] = t('Se connecter');
            $form['actions']['submit']['#attributes'] = array(
              'class' => array('btn-connexion')
            );
            $form['#suffix'] = '</div>
<div class="box">
<p>
Si vous avez oublié votre mot de passe :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user/password').$destination.'" class="fat-link">Demander un nouveau mot de passe</a>
</p>
<p>
Si vous n’avez pas de compte :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user/register').$destination.'" class="fat-link">Créer un nouveau compte</a>
</p>
</div>
</div> <!-- end .grid-fit -->';
            break;
        case 'user_pass':
        $form['#prefix'] = '<div class="grid-fit">
<div class="box">
<h1 class="page-name">nouveau mot de passe</h1>
<p>
Indiquez votre Email pour recevoir un courriel avec la procédure pour réinitialiser votre mot de passe.</p>
</div>
<div class="box">';
        $form['actions']['submit']['#value'] = t('Renvoyer le mot de passe');
        $form['actions']['submit']['#attributes'] = array(
          'class' => array('btn-lostpassword')
        );
        $form['#suffix'] = '</div>
<div class="box">
<p>
Si vous vous rappelez de vos identifiants :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user').$destination.'" class="fat-link"> Connectez vous à votre espace</a>
</p>
<p>
Si vous n’avez pas de compte :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user/register').$destination.'" class="fat-link">Créer un nouveau compte</a>
</p>
</div>
</div> <!-- end .grid-fit -->';
            break;
        case 'user_register_form':
          $form['#prefix'] = '<div class="grid-fit">
<div class="box">
<h1 class="page-name">inscription</h1>
<p>
<i class="txt-green fa fa-check" aria-hidden="true"></i> Inscription libre et gratuite<br><br>
<i class="txt-green fa fa-check" aria-hidden="true"></i> Nous nous engageons à protéger vos données personnelles<br><br> <i class="txt-green fa fa-check" aria-hidden="true"></i> Ni exploitation ni revente de vos données<br><br>
<i class="txt-green fa fa-check" aria-hidden="true"></i> Création automatique d\'un compte 360learning (ou mise en lien par l\'email )<br><br>
<img class="logo-360" src="'.$GLOBALS['base_url'] . '/' . path_to_theme().'/images/logo-360.png" alt="logo 360learning">
<a href="https://360learning.com/cgu" class="fat-link"><small>Voir les conditions générales d’utilisation de 360learning</small></a>
</p>
</div>
<div class="box">';
            $form['#suffix'] = '</div>
<div class="box">
<p>
Si vous êtes déjà inscrit :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user').$destination.'" class="fat-link"> Connectez vous à votre espace</a>
</p>
<p>
Si vous avez oublié votre mot de passe :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user/password').$destination.'" class="fat-link">Demander un nouveau mot de passe</a>
</p>
<br>
<p>
<h5>Si vous avez déjà suivi un MOOC de Colibris</h5>
Vous devez aussi vous créer un compte sur ce site. Le mot de passe peut être différent de celui de 360Learning, mais nous vous encourageons à utiliser le même. Vous retrouverez vos formations sur "Mon espace".
</p>
<p>
<p>
<h5>E-mail</h5>
L\'adresse électronique ne sera pas rendue publique et ne sera utilisée que pour la réception d\'un nouveau mot de passe ou pour la réception des notifications désirées. Le système enverra les courriels à cette adresse.
</p>
<p>
<h5>Adresse</h5>
Ce champ permettra de vous faire apparaitre comme participant aux moocs et de vous joindre par courrier. Les informations collectées ne seront pas rediffusées.
</p>
</div>
</div> <!-- end .grid-fit -->';
            break; 
        case 'user_profile_form': 
            $form['#prefix'] = '<div class="grid-fit">
<div class="box">
<h1 class="page-name">mon compte</h1>
<p>
<i class="txt-green fa fa-check" aria-hidden="true"></i> Changez vos informations facilement<br><br>
<i class="txt-green fa fa-check" aria-hidden="true"></i> Nous nous engageons à protéger vos données.
</p>
</div>
<div class="box">';
            $form['#suffix'] = '</div>
<div class="box">
<p>
Si vous souhaitez juste voir vos formations suivies :<br>
<i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
 <a href="'.drupal_get_path_alias('/user/').'" class="fat-link">Revenir à mon tableau de bord</a>
</p>
</div>
</div> <!-- end .grid-fit -->';
            break;
    }
}