<?php

/**
 * @file
 * Prepare variables for template files.
 */

/**
 * Implements template_preprocess_html().
 */
function colibris_preprocess_html(&$vars)
{
  // Responsive Support
  drupal_add_html_head(array(
    '#type'         => 'html_tag',
    '#tag'          => 'meta',
    '#attributes'   => array(
      'name'      => 'viewport',
      'content'   => 'width=device-width, initial-scale=1'
    )
  ), 'viewport');

  // XUA Support
  drupal_add_html_head(array(
    '#type'         => 'html_tag',
    '#tag'          => 'meta',
    '#attributes'   => array(
      'content'   => 'IE=edge',
      'http-equiv'=> 'X-UA-Compatible'
    )
  ), 'xua');

    if ($vars['is_admin']) {
        $vars['classes_array'][] = 'admin';
    }

    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    $temp = explode('/', $path, 2);
    $section = array_shift($temp);
    $page_name = array_shift($temp);

    if (isset($page_name)) {
        $vars['classes_array'][] = drupal_html_id('page-'.$page_name);
    }

    $vars['classes_array'][] = drupal_html_id('section-'.$section);

    if (arg(0) == 'node') {
        if (arg(1) == 'add') {
            if ($section == 'node') {
                array_pop($vars['classes_array']); // Remove 'section-node'
            }
            $vars['classes_array'][] = 'section-node-add'; // Add 'section-node-add'
        } elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
            if ($section == 'node') {
                array_pop($vars['classes_array']); // Remove 'section-node'
            }
            $vars['classes_array'][] = 'section-node-'.arg(2); // Add 'section-node-edit' or 'section-node-delete'
        }
    }
}

/**
 * Implements template_preprocess_page().
 */
function colibris_preprocess_page(&$vars)
{
    // Site navigation links.
    $vars['main_menu_links'] = '';
    if (!empty($vars['main_menu'])) {
        $main_menu_name = variable_get('menu_main_links_source', 'main-menu');
        $menu_tree = menu_build_tree($main_menu_name);
        $menu_tree_output = menu_tree_output($menu_tree);
        $menu_rendered = drupal_render($menu_tree_output);
        $vars['main_menu_links'] = $menu_rendered;
    }

    $secondary_menu_name = 'menu-sites-mouvement';
    $menu_tree = menu_build_tree($secondary_menu_name);
    $menu_tree_output = menu_tree_output($menu_tree);
    $menu_tree_output['#theme_wrappers'][] = 'menu_tree__secondary_menu';
    $menu_rendered = drupal_render($menu_tree_output);
    $vars['menu_sites'] = $menu_rendered;

    // Since the title and the shortcut link are both block level elements,
    // positioning them next to each other is much simpler with a wrapper div.
    if (!empty($vars['title_suffix']['add_or_remove_shortcut']) && $vars['title']) {
        // Add a wrapper div using the title_prefix and title_suffix render elements.
        $vars['title_prefix']['shortcut_wrapper'] = array(
          '#markup' => '<div class="shortcut-wrapper clearfix">',
          '#weight' => 100,
        );
        $vars['title_suffix']['shortcut_wrapper'] = array(
          '#markup' => '</div>',
          '#weight' => -99,
        );
        // Make sure the shortcut link is the first item in title_suffix.
        $vars['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
    }

    if (drupal_is_front_page()) {
        $vars['show_title'] = false;
        if (isset($vars['page']['content']['system_main']['default_message'])) {
            unset($vars['page']['content']['system_main']['default_message']);
        }
    }

    if (isset($vars['node'])) {
        $node = $vars['node'];

        //node themes
        $themes = field_get_items('node', $node, 'field_themes');
        if ($themes) {
            array_unshift($vars['page']['content_bottom'], field_view_field('node', $node, 'field_themes', 'dafault'));
        }

        //share links
        if (isset($node->service_links_rendered)) {
            array_unshift($vars['page']['content_bottom'], array('share' => array('#type' => 'markup', '#markup' => $node->service_links_rendered)));
        }

        //book pages siblings
        if ($node->type == 'main_page' && isset($node->book)) {
            array_unshift($vars['page']['content_bottom'], array('siblings' => array('#type' => 'markup', '#markup' => _book_siblings($node->book))));
        }
    }
}

/**
 * Implements template_preprocess_node().
 */
function colibris_preprocess_node(&$vars)
{
    // Striping class
    //$vars['classes_array'][] = 'node-'.$vars['zebra'];

    if ($vars['teaser']) {
        $vars['classes_array'][] = 'node-teaser'; // Node is displayed as teaser.
    }

    if ($vars['uid'] && $vars['uid'] === $GLOBALS['user']->uid) {
        $classes[] = 'node-mine'; // Node is authored by current user.
    }

    $vars['submitted'] = t('Submitted by !username on ', array('!username' => $vars['name']));
    $vars['submitted_date'] = t('published on').' '.format_date($vars['created'], 'long');
    $vars['submitted_pubdate'] = format_date($vars['created'], 'custom', 'Y-m-d\TH:i:s');

    if (isset($vars['view_mode'])) {
        $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'];
        $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'];
    }
}

/**
 * Implements template_preprocess_block_wrapper().
 */
function colibris_preprocess_menu_block_wrapper(&$vars)
{
    $vars['classes_array'] = array('menu-block-'.$vars['id']);
}

/**
 * Implement hook_preprocess_HOOK().
 */
function colibris_preprocess_field(&$variables) {
  // Inject the heart pictogram on the participation link field.
  if ($variables["element"]["#field_name"] == 'field_lien_vers_la_participation' && $variables["element"]["#entity_type"] == 'node' && $variables["element"]["#bundle"] == 'formation') {
    if (!empty($variables["items"][0]["#element"]["title"])) {
      $variables["items"][0]["#element"]["title"] = '<i class="fa fa-heart"></i> ' . $variables["items"][0]["#element"]["title"];
    }
  }
}

/**
 * Implements template_preprocess_block().
 */
function colibris_preprocess_block(&$vars, $hook)
{
    // Add a striping class.
    $vars['classes_array'][] = 'block-'.$vars['zebra'];

    $vars['title_attributes_array']['class'][] = 'block-title';

    // Use nav element for menu blocks and provide a suggestion for all of them
    $nav_blocks = array('navigation', 'main-menu', 'management', 'user-menu');
    $nav_modules = array('superfish', 'nice_menus', 'menu_block');
    if (in_array($vars['block']->delta, $nav_blocks) || in_array($vars['block']->module, $nav_modules)) {
        $vars['tag'] = 'nav';
        array_unshift($vars['theme_hook_suggestions'], 'block__menu');
    } elseif (!empty($vars['block']->subject)) {
        $vars['tag'] = 'section';
    } else {
        $vars['tag'] = 'div';
    }

  // In the header region visually hide block titles.
    if ($vars['block']->region == 'header') {
        if ($vars['id'] != 1) {
            $vars['title_attributes_array']['class'][] = 'element-invisible';
        }
    }
}

/**
 * Implements template_proprocess_search_block_form().
 *
 * Changes the search form to use the HTML5 "search" input attribute
 */
function colibris_preprocess_search_block_form(&$vars)
{
    $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

/**
 * Implements template_proprocess_preprocess_menu_local_task().
 *
 * Override or insert variables into theme_menu_local_task().
 */
function colibris_preprocess_menu_local_task(&$vars)
{
    $link = &$vars['element']['#link'];

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
        $link['title'] = check_plain($link['title']);
    }

    $link['localized_options']['html'] = true;
    $link['title'] = '<span class="tab">'.$link['title'].'</span>';
}

/**
 * Generate doctype for templates.
 */
function _colibris_doctype()
{
    return (module_exists('rdf')) ? '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN"'."\n".'"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">' : '<!DOCTYPE html>'."\n";
}

/**
 * Generate RDF object for templates.
 *
 * Uses RDFa attributes if the RDF module is enabled
 * Lifted from Adaptivetheme for D7, full credit to Jeff Burnz
 * ref: http://drupal.org/node/887600
 *
 * @param array $vars
 */
function _colibris_rdf($vars)
{
    $rdf = new stdClass();

    if (module_exists('rdf')) {
        $rdf->version = 'version="HTML+RDFa 1.1"';
        $rdf->namespaces = $vars['rdf_namespaces'];
        $rdf->profile = ' profile="'.$vars['grddl_profile'].'"';
    } else {
        $rdf->version = '';
        $rdf->namespaces = '';
        $rdf->profile = '';
    }

    return $rdf;
}

function colibris_html_head_alter(&$head_elements)
{
    foreach ($head_elements as $key => $element) {
        if (isset($element['#attributes']['rel']) && ($element['#attributes']['rel'] == 'canonical' || $element['#attributes']['rel'] == 'shortlink') && $element['#attributes']['href'] != '/') {
            unset($head_elements[$key]);
        }
    }
    drupal_add_html_head_link(array(
        'rel' => 'canonical',
        'href' => '/',
    ));
}
