<?php

/**
 * @file
 * Handle CSS & JS inclusion or exclusion.
 */
function colibris_js_alter(&$js) {

  // Move all scripts to the footer.
  // To force a file to go in the header do -
  // drupal_add_js('path/to/js', array('force_header' => TRUE));
    foreach ($js as &$item) {
      if (empty($item['force_header'])) {
        $item['scope'] = 'footer';
      }
    }
}


/**
 * Implements hook_js_alter().
 */
function colibris_css_alter(&$css) {
  //remove unwanted core & modules stylesheets
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
}
