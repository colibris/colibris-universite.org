(function($) {
  $(document).ready(function () {
    /* on transforme les labels en placeholder */
    $('#sendinblue-signup-subscribe-block-lettre-des-colibris-form :input, #user-register-form :input, #user-profile-form :input, #user-login :input, #user-pass :input').each(function (index, elem) {

      var eId = $(elem).attr('id');
      var label = null;
      if (eId && (label = $(elem).parents('form').find('label[for=' + eId + ']:not([for="edit-picture-upload"],[for="edit-picture-delete"])')).length == 1) {
        $(elem).attr('placeholder', $(label).text());
        $(label).hide();
      }
    });

    $('.views-exposed-form .form-select').not('.niced').addClass('niced').niceSelect();

    $('.card-formation .group-info.field-group-card').click(function() {
      window.location = $(this).find('.title a').attr('href');
    });

    // Build the user profile trainings list tabs.
    var tab = document.getElementById('user-trainings-list-tabs');
    if (tab) {
      $("#user-trainings-list-tabs").tabs();
    }

    // menu toggle for mobile view
    $('.menu-toggle').click(function() {
      $('#navbarToggler').toggleClass( "show" );
      return false;
    })
  });
})(jQuery);
