<?php
//$theme_path = path_to_theme();
$theme_path = 'sites/all/themes/custom/colibrisuniversite';

require_once $theme_path . '/includes/assets.inc';
require_once $theme_path . '/includes/preprocess.inc';
require_once $theme_path . '/includes/theme.inc';
