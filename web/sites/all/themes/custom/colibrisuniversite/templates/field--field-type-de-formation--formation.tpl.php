<?php foreach ($items as $delta => $item) : ?>
<?php
$formation = '';
if ($element['#view_mode'] == 'card') {
    $formation = ' '.strtolower(render($item));
}
?>
<div class="<?php print $classes.$formation; ?>"<?php print $attributes; ?>>
  <div class="field-items"<?php print $content_attributes; ?>>
    <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></div>
    </div>
</div>
<?php endforeach; ?>
