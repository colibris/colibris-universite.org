<?php 
  if(isset($node->field_is_video['und']) && $node->field_is_video['und'][0]['value'] == 1) {
    $type_class = ' video';
  } else {
    $type_class = '';
  }
?>
<article class="child medium<?php print $type_class;?>">
  <?php print render($content['field_image']); ?>
  <div class="text">
    <?php print render($title_prefix); ?>
    <h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php print render($title_suffix); ?>
    <?php
      if(isset($content['field_teaser'])) {
        print render($content['field_teaser']); 
      } else {
        print render($content['field_body']); 
      }

     if (isset($content['field_rub']) || isset($content['field_themes'])) {
        $links = array();
        if(isset($content['field_rub'])) {
          $links[] = render($content['field_rub']); 
        }
        if(isset($content['field_themes'])) {
          $links[] = render($content['field_themes']); 
        }
        print '<div class="terms">' . implode(' / ', $links) . '</div>';
     }
    ?>
  </div>
</article>