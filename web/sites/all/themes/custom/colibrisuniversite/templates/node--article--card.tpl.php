<article id="node-<?php print $node->nid; ?>" class="card card-article <?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content" <?php print $content_attributes; ?>>
    <?php
      // We hide the comments, tags and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);

      print render($content);
    ?>
  </div><!-- /.content -->
</article><!-- /.node -->
