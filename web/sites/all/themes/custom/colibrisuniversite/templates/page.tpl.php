<header id="header" role="banner">
  <div id="archipel-colibris" class="archipel" role="banner">
  </div>
  <script>
const url =  'https://colibris-lemouvement.org/archipel-markup?domain=colibris-universite.org'
var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};

getJSON(url,
function(err, data) {
  if (err !== null) {
    console.log('Something went wrong: ' + err);
  } else {
    document.getElementById('archipel-colibris').innerHTML = data.markup;
    var styleElement = document.createElement("style");
    styleElement.innerHTML = data.style;
    document.getElementById('archipel-colibris').appendChild(styleElement);
  }
});

</script>
<!--
Bannière Campagne


  <a href="https://soutenir.colibris-lemouvement.org" style="display:block;">
    <img class="img-responsive" src="https://www.colibris-lemouvement.org/sites/all/themes/colibris/images/AADmid23_bandeau_site.png"
   alt="Soutenez le Mouvement Colibris !">
  </a>

Fin Bannière Campagne
-->


<!--
Bannière AàD
-->


<center><a href="https://soutenir.colibris-lemouvement.org/" style="display:block;">
<?php
// Tableau contenant les noms des images
$images = array("aad2023-bannersite-1.jpg", "aad2023-bannersite-2.jpg");

// Choix aléatoire d'une image
$image_choisie = $images[array_rand($images,1)];

// Affichage de l'image dans la page HTML
echo '<img class="img-responsive" src="https://www.colibris-lemouvement.org/sites/all/themes/colibris/images/' . $image_choisie . '"  alt="Soutenez le Mouvement Colibris !">';
?>
</a>
</center>
<!--
Fin Bannière AàD
-->


  <nav class="navigation">
    <div class="wrapper clearfix">
      <div class="menu-flex">
        <a class="menu-toggle navbar-toggler" href="#" title="Toggle navigation">
          <i class="fa fa-bars" aria-hidden="true"></i> MENU
        </a>

        <a class="logo-custom" href="<?php print $front_page; ?>" title="page d'accueil Colibris Université"><img src="<?php echo $GLOBALS['base_url']; ?>/sites/all/themes/custom/colibrisuniversite/images/Logo_Colibris_RVB.svg" alt="Logo Colibris Université" /></a>

        <div id="navbarToggler" class="main-menu">
            <?php echo $main_menu_links; ?>
        </div>
        <div class="user-menu">
        <?php if (user_is_logged_in()) : ?>
          <a class="button yellow" href="<?php echo $GLOBALS['base_url']; ?>/user/<?php echo $GLOBALS['user']->uid; ?>">
            <i class="fa fa-user" aria-hidden="true"></i> Mon espace
          </a>
          <a class="logout" href="<?php echo $GLOBALS['base_url']; ?>/user/logout">
            <i class="fa fa-sign-out" aria-hidden="true"></i> Déconnexion
          </a>
        <?php else : ?>
          <a class="button yellow" href="<?php echo $GLOBALS['base_url']; ?>/user">
            <i class="fa fa-sign-in" aria-hidden="true"></i> Connexion
          </a>
          <a class="login" href="<?php echo $GLOBALS['base_url']; ?>/user/register">
            <i class="fa fa-user-plus" aria-hidden="true"></i> S'inscrire
          </a>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </nav>
  <?php
  if (!empty($messages)) {
      echo '<div class="wrapper">'.$messages.'</div>';
  }
  print render($page['header']);
  ?>
</header>
<div id="page">
  <div id="main">
    <div class="wrapper">
        <?php if ($page['content_sidebar']) : ?>
        <div class="flex-2cols">
          <div class="sidebar">
            <?php echo render($page['content_sidebar']);?>
          </div>
          <?php endif; ?>
          <?php echo render($page['content']); ?>
          <?php if ($page['content_sidebar']) : ?>
            </div>
          <?php endif; ?>
          <?php if (false and $action_links) : ?>
          <ul class="action-links">
            <?php echo render($action_links); ?>
          </ul>
        <?php endif; ?>
        <?php if ($tabs) : ?>
          <div id="tab-links">
            <?php echo render($tabs); ?>
          </div>
        <?php endif; ?>
    </div>
  </div>

    <?php if ($page['content_bottom']) : ?>
    <footer id="content_bottom" role="contentinfo">
      <div class="wrapper">
        <?php echo render($page['content_bottom']); ?>
      </div>
    </footer>
    <?php endif; ?>

    <?php if ($page['footer']) : ?>
    <footer id="footer" role="contentinfo">
      <div class="wrapper">
        <?php echo render($page['footer']); ?>
      </div>
    </footer>
    <?php endif; ?>
</div>
