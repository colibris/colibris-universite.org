<article class="child large">
  <div class="row">
    <div class="col half">
    <?php print render($content['field_image']); ?>
    </div>
    <div class="col half text">
      <?php print render($title_prefix); ?>
      <h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php print render($title_suffix); ?>
      <?php
        if(isset($content['field_teaser'])) {
          print render($content['field_teaser']); 
        } else {
          print render($content['field_body']); 
        }
      ?>
    </div>
  </div>
</article>