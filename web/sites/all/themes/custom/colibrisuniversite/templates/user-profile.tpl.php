<?php
if (!function_exists('array_key_first')) {
  function array_key_first(array $arr) {
    foreach ($arr as $key => $unused) {
      return $key;
    }
    return NULL;
  }
}
?>
<div class="apprenant profile"<?php print $attributes; ?>>
  <div class="group-one-third">
    <h1 class="page-name">tableau de bord</h1>
    <?php
    hide($user_profile['field_training_sessions']);
    print render($user_profile);

    $current_user = menu_get_object('user');
    $cas_username = cas_current_user();
    if ($cas_username) {
      $cas_attributes = cas_phpcas_attributes($cas_username);
      $profil_uid = $cas_attributes['uid'];
      // Don't like it, but can't find a better way of doing it.
      $casUrl = 'https://' . variable_get('cas_server');
      //$destination = url('user/' . $currentuser->uid, ['absolute' => TRUE]);
      $destination = url('cas_relog', ['absolute' => TRUE]);
      $editUserUrl = $casUrl . '/user/' . $profil_uid . '/edit?redirect=' . $destination;
    }
    else {
      $editUserUrl = drupal_get_path_alias('user/'.$current_user->uid.'/edit');
    }

    ?>
    <a class="button yellow edit-user" href="<?php print $editUserUrl; ?>">
      <i class="fa fa-pencil" aria-hidden="true"></i> Modifier mon compte
    </a>
    <a class="button red cancel-user" href="/<?php print drupal_get_path_alias('user/'.$current_user->uid.'/cancel'); ?>">
      <i class="fa fa-times" aria-hidden="true"></i> Annuler mon compte
    </a>
  </div>
  <div class="group-two-third">
    <h2 class="welcome">Bienvenue sur votre tableau de bord</h2>
    <p>Cet espace vous permet de visualiser les formations que vous avez entamées ou auxquelles vous vous êtes inscrits. </p>
    <?php if (!isset($user_profile['field_training_sessions'], $user_profile['field_training_sessions']['#object']->field_training_sessions[LANGUAGE_NONE]) || count($user_profile['field_training_sessions']['#object']->field_training_sessions[LANGUAGE_NONE]['subscribed']) == 0) : ?>
      <h3>Mes formations</h3>
      <p>
        <strong><em>Vous n'avez pas encore d'inscription à une formation associée à votre compte.</em></strong>
      </p>
      <p>
        Vous pouvez <a href="<?php print drupal_get_path_alias('formations'); ?>"> consulter notre catalogue des formations</a> pour suivre des cours et y accéder par ce tableau de bord par la suite.
      </p>
    <?php
    else:
      print render($user_profile['field_training_sessions']);
    endif;
    ?>
  </div>
</div>
