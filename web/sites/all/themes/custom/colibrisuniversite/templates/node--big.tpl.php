<article class="child big">
  <?php print render($content['field_image']); ?>
  <div class="text">
    <?php print render($title_prefix); ?>
    <h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php print render($title_suffix); ?>
    <?php
      if(isset($content['field_teaser'])) {
        print render($content['field_teaser']); 
      } else {
        print render($content['field_body']); 
      }
    ?>
  </div>
</article>