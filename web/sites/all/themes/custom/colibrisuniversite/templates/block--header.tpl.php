<?php

/**
 * Template du bandeau
 */

$currentNode = menu_get_object();

if ($currentNode) {
    $title = $currentNode->title;
    if (property_exists($currentNode, 'field_image') && is_array($currentNode->field_image)) {
        // bandeau personnalise
        $url = file_create_url($currentNode->field_image['und'][0]['uri']);
    } else {
        // bandeau par defaut
        $url = $GLOBALS['base_url'].'/'.path_to_theme() . '/images/bandeau-default.jpg';
    }
} else {
    // bandeau par defaut
    $url = $GLOBALS['base_url'].'/'.path_to_theme() . '/images/bandeau-default.jpg';
    $title = drupal_get_title();
}
?>
<?php if (!drupal_is_front_page()) : ?>
<header class="heading">
    <div class="wrapper">
        <h1 class="page-name"><?php echo $title; ?></h1>
    </div>
</header>
<?php else : ?>
<?php
//$nodes = node_load_multiple(array(), array('type' => 'zoom-sur-accueil'));
$query = db_select('node', 'n');
$query->condition('n.type', 'zoom_sur_accueil');
$query->condition('n.status', NODE_PUBLISHED);
$nids = $query->fields('n', array('nid'))
    ->orderBy('n.changed', 'DESC')
    ->range(0, 1)
    ->addTag('node_access')
    ->execute()
    ->fetchCol();
$nodes = node_load_multiple($nids);
$infos = array_pop($nodes);
$iframe = '';

if ($infos->field_image_de_fond['und'][0]['filemime'] == 'video/vimeo') {
    $vid = explode('vimeo://v/', $infos->field_image_de_fond['und'][0]['uri']);
    $iframe = '<div class="responsive-iframe"><iframe src="https://player.vimeo.com/video/'.$vid[1].'?title=0&byline=0&portrait=0" width="640" height="338" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
    <a class="button yellow pull-right" href="'.$infos->field_lien['und'][0]['safe_value'].'">Plus d\'information</a>
    <h2>'.$infos->title.'</h2>';
} elseif ($infos->field_image_de_fond['und'][0]['filemime'] == 'video/youtube') {
    $vid = explode('youtube://v/', $infos->field_image_de_fond['und'][0]['uri']);
    $iframe = '<div class="responsive-iframe">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/'.$vid[1].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
    <a class="button yellow pull-right" href="'.$infos->field_lien['und'][0]['safe_value'].'">Plus d\'information</a>
    <h2>'.$infos->title.'</h2>';
}
$url = file_create_url($infos->field_image_de_fond['und'][0]['uri']);
?>
<section class="heading-home">
    <div class="wrapper">
        <div class="about">
            <!--style="background-image: url();background-size:cover;background-repeat:no-repeat;"> -->
            <h1>Bienvenue<br>dans <strong>l'université</strong><br>des <strong>colibris</strong></h1>
            <p>Colibris vous accompagne dans vos projets ! Avec l'Université des Colibris, nous réunissons des experts de la transition, dans de nombreux domaines : éducation, agriculture, habitat participatif… Ils partagent leurs savoirs, afin que vous puissiez vous former, et contribuer ainsi à une société plus écologique et solidaire.</p>

            <?php
                if (isset($infos->body['und'][0]['safe_value']) and !empty(trim($infos->body['und'][0]['safe_value']))) {
                    echo '<br>'.$infos->body['und'][0]['safe_value'];
                }
            ?>
        </div>
        <div class="hero">
            <?php if (!empty($iframe)) : echo $iframe; ?>
            <?php else : ?>
            <a titre="<?php echo $infos->title; ?>" href="<?php echo $infos->field_lien['und'][0]['safe_value']; ?>">
                <img style="width:100%" class="img-rounded" src="<?php echo $url; ?>" alt="image en une">
            </a>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endif; ?>
