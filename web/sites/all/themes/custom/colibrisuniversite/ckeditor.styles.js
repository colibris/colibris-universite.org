if(typeof(CKEDITOR) !== 'undefined') {
  CKEDITOR.addStylesSet( 'drupal',
  [
    /* Block Styles */
    { name : 'Paragraph'		, element : 'p' },
    { name : 'Heading 2'		, element : 'h2' },
    { name : 'Heading 3'		, element : 'h3' },
    { name : 'Heading 4'		, element : 'h4' },
  ]);
}