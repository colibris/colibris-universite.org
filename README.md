# Colibris

## Installation

    composer install
     
    cd web
    ../vendor/bin/drush make ../colibris.make

Follow the drupal installation process by accessing the website (example: http://local.colibris-universite.org)