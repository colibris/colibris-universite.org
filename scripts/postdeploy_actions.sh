#!/bin/bash
# Action to run after the main and shared deployment actions.
# It can be useful to enable specific modules for instance.
# Available variables are defined in settings.sh.

# Return error codes if they happen.
set -e

case $COMBAWA_ENV in
  dev)
    $DRUSH en diff devel stage_file_proxy colibris_environments
    $DRUSH vset stage_file_proxy_origin https://colibris-universite.org

    # Connect.
    $DRUSH uli
    ;;
  recette|preprod)
    $DRUSH en stage_file_proxy colibris_environments
    $DRUSH vset stage_file_proxy_origin https://colibris-universite.org
    ;;
  prod)
    $DRUSH dis devel
    ;;
  *)
    notify_error "Unknown environment: $COMBAWA_ENV. Please check your name."
esac
