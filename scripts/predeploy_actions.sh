#!/bin/bash
# Actions to run before the main and shared deployment actions.
# It can be useful to backup, import databases or doing something similar.
# Available variables are defined in settings.sh.

# Return error codes if they happen.
set -e

if [ "$COMBAWA_FETCH_DB_DUMP" == "1" ] ; then
  download_dump
fi

case $COMBAWA_ENV in
  dev)
    # In update mode, load the reference dump if it exists.
    if [ "$COMBAWA_BUILD_MODE" == "update" ]; then
      load_dump
    fi
    # TMP fix to force dependencies to be enabled before resetting features.
    $DRUSH en -y colibris_ft_site_universite
    ;;
  recette|preprod)
    # In update mode, load the reference dump if it exists.
    if [ "$COMBAWA_BUILD_MODE" == "update" ]; then
      load_dump
    fi
    # TMP fix to force dependencies to be enabled before resetting features.
    $DRUSH en -y colibris_ft_site_universite
    ;;
  prod)
    # TMP fix to force dependencies to be enabled before resetting features.
    $DRUSH en -y colibris_ft_site_universite
    ;;
  *)
    notify_error "Unknown environment: $COMBAWA_ENV. Please check your name."
esac
