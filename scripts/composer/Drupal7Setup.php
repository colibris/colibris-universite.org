<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace Colibris\composer;

use Composer\Script\Event;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;

class Drupal7Setup {

  public static function includeSettingsLocal(Event $event) {
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();
    $settings_file = $drupalRoot . '/sites/default/settings.php';

    $io = $event->getIO();

    // If we have the settings file & local settings file.
    if ($fs->exists($settings_file) && $fs->exists($drupalRoot . '/sites/default/settings.local.php')) {
      $content = "// Include local settings.\n";
      $content .= 'require_once __DIR__ . \'/settings.local.php\';';
      $content .= "\n";

      // Get the settings file.
      $settings_content = file_get_contents($settings_file);

      // And check if already includes the local settings file.
      if (!preg_match("`$content`", $settings_content)) {
        // If not, add the content to the file!
        file_put_contents($settings_file, $content, FILE_APPEND);
      }
    }
  }
}
